Guide De Programmation
======================

  *Ce guide rapide est à lire avant de se lancer dans la programmation du projet tutoré.*
  *Il se propose de rappeler à tous, quelques règles pratiques pour conserver un projet cohérent et agréable.*


1. Les fichiers et leur en-tête
-------------------------------

  Chaque fichier devra comporter un **en-tête** comportant plusieurs informations (notées dans l'ordre):
  ..1. La matière du projet              --     *Projet Tutoré - L3 Info*
  ..2. Le titre de contenu du fichier    --     *TAD ARBRE*
  ..3. Le nom de chaque membre du groupe --     *Clain-Januel-Laroche-Moulin-Verdun*

  Note: Les titre de contenu ci-dessus est donné à titre d'exemple.


2. Les Cartouches
-----------------

  Toute fonction se verra accompagnée d'une **cartouche** suivant le modèle ci-dessous:
  ```php
  /**
   * Description de la fonction
   * 
   * @param int 'param1', char 'param2'   ->  Description éventuelle des paramètres
   * @return int 'valeur'                 ->  Description éventuelle de la variable renvoyée
   **/
  ```

  De plus, il **ne faut pas hésiter** à ajouter des commentaires suivants ce modèle (__/**  ...  **/__)
  pour éclaircir certaines parties de fonctions.


3. Les commentaires linéaires
-----------------------------
  
  Les commentaires suivants le modèle (__//__), peuvent être utilisés de manières **temporaire**.
  En effet, ils sont voués à **disparaître** lors du nettoyage du code.
  Toutefois, ils peuvent être utiles en complément aux outils mis à disposition (GIT, KANBAN, etc),
  pour localiser une zone problématique, ou en cours de travail du code. 


*Bon travail, et bon courage!*