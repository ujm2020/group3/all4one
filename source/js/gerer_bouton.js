  /**************************************************/
  /***             Projet tutoré L3 info          ***/
  /***               JavaScript Smartphone        ***/
  /*** Clain-Januel-Laroche-Moulin-Verdun-Buzenet ***/
  /**************************************************/

const CHOIX = 0;
const ACTION = 1;

var tab=[];
var tab_stuff=[];
var score=[];
var totalSeconds;
var i=0;
// Si verif_click est a 0 alors aucun choix n'a été fait par l'utilisateur
var verif_click=0;
var salle_actuelle=0;
var type = CHOIX;

var nb_objet = 0;
const NB_MAX_OBJET = 16;
// booleen de premiere connexion
var premiere_co = 1;


$(document).ready(function(){
    StartTimer();
    reset();
});


/**
 * fonction pour lancer la décompte du timer
 * 
 */

function StartTimer(){
    rec_data();
    totalSeconds+=5;
    setInterval(Timer_Tick, 1000);
    document.getElementById("Timer").innerHTML = totalSeconds;
    document.getElementById("Timer").innerHTML = totalSeconds;
}

/* fonction qui recupere le chrono du joueur */
function rec_data(){
    $.ajax({
        type: 'POST',
        url: '../php/recupe_data.php',
        success: function(data){
            info=JSON.parse(data);
            salle_actuelle = info[0];
            totalSeconds = info[1];
        },
        error: function(){
            alert('Erreur sauvegarde partie js');
        }
    });
}

/**
 * fonction executer toute les 1s qui vérifie si le compteur est terminer, donc si le vote est terminer.
 * Des traitements sont réaliser lorsque le compteur arrive a 0 telle que le réaffichage des boutons
 * 
 */

function Timer_Tick(){
    if(salle_actuelle == 44){
        document.location.href = "../php/fin_smartphone.php";
    }
    
    if(totalSeconds > 5){
        totalSeconds--;   
        document.getElementById("Timer").innerHTML = totalSeconds;
    }else{
        if(totalSeconds > 0){
            totalSeconds--;   
            document.getElementById("Timer").innerHTML = totalSeconds;
        }
        
        if(verif_click==0){
            //choix random de la réponse
            recup_vote(tab[Math.floor(Math.random()*tab.length)]);
          
        }

        /**
         * Ici demandeServeur
         */
        totalSeconds--;
        if(totalSeconds<=-5){
            reset();
            rec_data();
            totalSeconds+=5;
        }
    }
}

/**
 * envoie le vote récuperer au serveur pour qu'il puisse traiter se dernier
 * @param choix
 */

function recup_vote(choix){
    var envoi=JSON.stringify(choix.name);
    var envoi2=JSON.stringify(choix.value);
    var envoi3=JSON.stringify(salle_actuelle);

    if(verif_click==1){
        $.ajax({
            type: 'POST',
            url: '../php/save_vote.php',
            data: 'choix='+envoi+'&choix2='+envoi2+'&salle_actuelle='+envoi3+'&type='+type,
            success: function(){
                verif_click=1;
            },
            error : function(){
                console.log("Echec envoi vote choisis");
            }
        });
    }
    else{
        if(type==CHOIX){
            envoi=JSON.stringify(choix.idChoix);
            envoi2=JSON.stringify(choix.label);
        }else{
            envoi=JSON.stringify(choix.idAction);
            envoi2=JSON.stringify(choix.label);
        }
        $.ajax({
            type: 'POST',
            url: '../php/save_vote.php',
            data: 'choix='+envoi+'&choix2='+envoi2+'&salle_actuelle='+envoi3+'&type='+type,
            success: function(){
                //alert('Réussite envoi choix user random');
                verif_click=1;
            },
            error : function(){
                console.log("Echec envoi vote aléatoire");
            }
        });
    }
}

/**
 * Fonction qui affiche les choix envoyer par le serveur
 * @param p  -> paragraphe
 */
function afficheVote(p){
    var html="";
    if(typeof p[0].idChoix != "undefined"){
        for (var item in p) {
            html+="<input type=\"button\" name=\""+p[item].idChoix+"\" value=\""+p[item].label+"\" onclick=\"valider(this)\" ></input>"; 
        }
    }else{
        for (var item in p) {
            html+="<input type=\"button\" name=\""+p[item].idAction+"\" value=\""+p[item].label+"\" onclick=\"valider(this)\" ></input>"; 
        }
    }
    $('#le_p').html(html);
}

function valider(choix){
    if(verif_click!=1){
        verif_click=1;
        recup_vote(choix);
    }
}

/**
 * Fonction permettant de demander les choix d'affichage pour le client
 * 
 */

 /**
  * Récup des données ==> 
  * Liste de choix/action selon salle(via sauvegarde)
  */
function demandeServeur(){
    $.ajax({
        type: 'POST',
        url: '../php/next.php',
        success: function(data){
            /**
             * Décodage value
             */
            tab=JSON.parse(data);
            salle_actuelle=tab[0].fkSalle;
            if(tab[0].action == 0)
                type=CHOIX;
            else{
                type=ACTION;
            }
            gerer_stuff();
            afficheVote(tab);
            affScorePvSm();
        },
        error : function(){
            console.log("ERROR!!!\n\nERROR!\n");
        }
    });
}

/**
 * fonction demande du serveurs et d'affichage
 * 
 */
function reset(){
    verif_click=0;
    demandeServeur();
}

/* ajoute un element au suff stuff */
function gerer_stuff(){
    $.ajax({
        type: 'POST',
        url: '../php/stuff.php',
        success: function(data){
            tab_stuff=JSON.parse(data);
            nb_objet = tab_stuff.length;

            /* mise a jour affichage stuff */
            for(i = 0; i<NB_MAX_OBJET; i++){
                if(i<nb_objet){
                    $('#td'+i).html('<img src="../../img/'+tab_stuff[i].objet+'" alt="'+tab_stuff[i].objet+'" >');
                }else{
                    $('#td'+i).html('');
                }
            }
        },
        error : function(){
            console.log("echec stuff");
        }
    });
}

/**
 * Affichage score, pv et sm
 */
function affScorePvSm(){
    var id_perso = $("#nom_perso").attr('title');
    var perso = JSON.stringify(parseInt(id_perso));
    $.ajax({
        type: 'POST',
        url: '../php/affScorePvSm.php',
        data: 'perso='+perso,
        success: function(data){
            score = JSON.parse(data);

            $("#pv").html("PV : "+score[0].pv);
            $("#sm").html("SM : "+score[0].santeMental);
            $("#score").html("Score : "+score[0]["score"]);
        },
        error : function(){
            console.log("pas ok--> Affichage");
        }
    });
}
