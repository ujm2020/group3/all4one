  /**************************************************/
  /***             Projet tutoré L3 info          ***/
  /***               JavaScript Carte             ***/
  /*** Clain-Januel-Laroche-Moulin-Verdun-Buzenet ***/
  /**************************************************/

var tab=[];
var tab2=[];
var chargement=[];
var carte, position_salle;
var NB_COLONNE=34, NB_LIGNE=28, NB_SALLE=45;
var pv = 20, sm = 20;
var xp, yp; //position du personnage qui suit la direction de xtmp
var xtmp, ytmp; //coordonne temporaire qui parcours chaque case du chemin
var xmat, ymat; //coordonne qui permet de savoir dans quelle case de la matrice on se trouve 
var xmatmp, ymatmp; //coordonne poru savoir si on est deja passe dessus
var tmp_attente = 35; // temps que le joueur a pour faire son choix
var totalSeconds = tmp_attente;
//variable pour placer le personnage et la taille du halo
var OFFSET_TOP=222;
var OFFSET_LEFT=22;
var radius=100;

var salle_actuelle=1; 
var salle_arrive;
var pattern;
var num_chemin;

var h_case, w_case;

//variable pour le texte 
var chaine = "";
var text_dep = "Krane An Katr un mal vous gagne peu à peu, la folie s'empare de vous.";
text_dep += "Pour parer à ce maléfice vous devez récuperer l'élixir de Tutenva. Il se trouve aprés les grottes, au sommet de la montagne dans une tour.";

//variable pour les son 
var son="../../son/foret.mp3";
var audio;
var musique=new Audio("../../son/foret.mp3");

var totalSeconds;

function showText (target, message, index, interval) {    
    if (index < message.length) { 
        $(target).append(message[index++]); 
        setTimeout(function () { showText(target, message, index, interval); }, interval); 
    } 
}

function cleanText(message){
    document.getElementById("msg").innerHTML = message;
}

function chargerImage(image){
    $("#cadreImage").attr("src",image);
}



/* premier appel lance le chargement et la gestion de la carte */
requete();

function requete(){
    $.ajax({
        type: 'POST',
        url: '../php/creation_carte.php',
        success: function(data){
            tab=JSON.parse(data);
            creer_carte();
          
            var h_canvas=document.getElementById('canvas').height, w_canvas=document.getElementById('canvas').width;
            h_case=h_canvas/NB_LIGNE, w_case=w_canvas/NB_COLONNE;
            
            salle_actuelle = 1;
            xp=position_salle[salle_actuelle][0]*w_case;
            yp=position_salle[salle_actuelle][1]*h_case;
          
            perso.style.height=h_case+"px";
            perso.style.width=w_case+"px";
            
            perso.style.top = yp+OFFSET_TOP+"px";
            perso.style.left = xp+OFFSET_LEFT+"px";
            chargerImage("../../img/depart.png");
            showText("#msg",text_dep,0,10);
            draw_fog();
            
            musique.loop=true;
            musique.play();

            //jouer_son(); //voix de lecture de départ
            //charger();
            StartTimer();
        },
        error: function(){
            alert('ca marche po');
        }
    });
}

function lancement_jeu(){
    var mss=[];
    //requete ajax
    $.ajax({
        type: 'POST',
        url: '../php/choix.php',
        data: 'salle_actuelle='+salle_actuelle+'&pv='+pv+'&sm='+sm,
        success: function(data){
            //alert('reussi');
            tab2=JSON.parse(data);
            cleanText(chaine);

            xp=position_salle[salle_actuelle][0]*w_case;
            yp=position_salle[salle_actuelle][1]*h_case;
            
            num_chemin=tab2[0].numChemin;
            /**
             * Action ou Choix
             */
            if(tab2[0].action == 0)
                salle_arrive=tab2[0].fkSalleSuiv;   
            else
                salle_arrive=tab2[0].fkSalle;
        
            
            msg.innerHTML = "</br>"
            chaine = tab2[0].histoire;
            pv = tab2[0].pv;
            sm = tab2[0].sm;

            chargerImage(tab2[0].image_name);
            showText("#msg", chaine, 0, 10);
            
            if(salle_actuelle==21 || salle_actuelle==22){
                musique.pause();
                musique= new Audio("../../son/grotte.mp3");
                musique.play();
            }
            //Voix de lecture
            /*son=tab2[0].son;
            jouer_son();*/

            if(num_chemin==0){
              //alors action
            }else{
                deplace_perso();
                salle_actuelle=salle_arrive;
            }
        },
        error: function(){
            console.log('ca marche po lancement jeu');
        }
    }); 
}


function creer_carte(){
    carte=new Array(NB_COLONNE);
    for(var i=0; i<NB_COLONNE; i++){
        carte[i]= new Array(NB_LIGNE);
    }
    for(i=0; i<NB_COLONNE; i++){
        for(var j =0; j<NB_LIGNE; j++){
            carte[i][j]=0;
        }
    }
    position_salle=new Array(NB_SALLE);
    for(i=0; i<NB_SALLE; i++){
        position_salle[i]= new Array(2);
    }
    for(i=0; i<NB_SALLE; i++){
        for( j=0; j<2; j++){
            position_salle[i][j]=0;
        }
    }
    carte[1][14]=1;
    position_salle[1][0]=1;
    position_salle[1][1]=14;
    
    for(var item in tab){
        salle_actuelle=tab[item].fkSalle;
        salle_arrive=tab[item].fkSalleSuiv;
        pattern=tab[item].numPattern;
        num_chemin=tab[item].numChemin;
        x=position_salle[salle_actuelle][0];
        y=position_salle[salle_actuelle][1];

        switch(pattern){
            //pattern 1  : 3 case à droite : aller a l'est
            case "1":
            for(i=x+1; i<x+3; i++){
                carte[i][y]=num_chemin;
            }
            carte[i][y]=salle_arrive;
            position_salle[salle_arrive][0]=i;
            position_salle[salle_arrive][1]=y;
            break;

          
            //pattern 2  : 4 cases en haut et 3 cases a droite
            case "2":
            for(i=y-1; i>=y-4; i--){
                carte[x][i]=num_chemin;
            }
            i++;
            for(j=x+1; j<x+3; j++){
              carte[j][i]=num_chemin;
            }
            carte[j][i]=salle_arrive;
            position_salle[salle_arrive][0]=j;
            position_salle[salle_arrive][1]=i;
            break;

          
            //pattern 3  : 4 cases en bas et 3 cases a droite
            case "3":
            for(i=y+1; i<=y+4; i++){
                carte[x][i]=num_chemin;
            }
            i--;
            for(j=x+1; j<x+3; j++){
                carte[j][i]=num_chemin;
            }
            carte[j][i]=salle_arrive;
            
            position_salle[salle_arrive][0]=j;
            position_salle[salle_arrive][1]=i;
            break;

          
            //pattern 4  : Both : Sud-Nord 
            case "4":
            for(i=y+1; i<=y+3; i++){
                carte[x][i]=num_chemin;
            }
            carte[x][i]=salle_arrive;
            position_salle[salle_arrive][0]=x;
            position_salle[salle_arrive][1]=i;
            break;

            //pattern 5  : Both : Nord-Est - Sud-Ouest
            case "5":
            for(i=y-1; i>=y-2; i--){
                carte[x][i]=num_chemin;
            }
            i++;
            y=i;
            for(j=x+1; j<=x+3; j++){
                carte[j][i]=num_chemin;
            }
            j--;
            
            for(i=y-1; i>y-2; i--){
                carte[j][i]=num_chemin;
            }
            carte[j][i]=salle_arrive;
            position_salle[salle_arrive][0]=j;
            position_salle[salle_arrive][1]=i;
            break;
            
            //pattern 6  Sud-Est grand angle
            case "6":
            for(j=x+1; j<=x+3; j++){
                carte[j][y]=num_chemin;
            }
            j--;
            for(i=y+1; i<y+4; i++){
                carte[j][i]=num_chemin;
            }
            carte[j][i]=salle_arrive;
            position_salle[salle_arrive][0]=j;
            position_salle[salle_arrive][1]=i;
            break;

          
            //pattern 7  nord-Est grand angle
            case "7":
            for(j=x+1; j<=x+3; j++){
                carte[j][y]=num_chemin;
            }
            j--;
            for(i=y-1; i>=y-7; i--){
                carte[j][i]=num_chemin;
            }
            carte[j][i]=salle_arrive;
            position_salle[salle_arrive][0]=j;
            position_salle[salle_arrive][1]=i;
            break;

            //pattern 8  Grand Sud
            case "8":
            for(i=y+1; i<=y+7; i++){
                carte[x][i]=num_chemin;
            }
            carte[x][i]=salle_arrive;
            position_salle[salle_arrive][0]=x;
            position_salle[salle_arrive][1]=i;
            break;


            //pattern 9  Nord-Est angle bas
            case "9" :
            for(j=x+1; j<=x+3; j++){
                carte[j][y]=num_chemin;
            }
            j--;
            for(i=y-1; i>y-4; i--){
                carte[j][i]=num_chemin;
            }
            carte[j][i]=salle_arrive;
            
            position_salle[salle_arrive][0]=j;
            position_salle[salle_arrive][1]=i;
            break;

            //pattern 10 nord est mal foutu
            case "10" :
            carte[x][y-1]=num_chemin;
            carte[x][y-2]=num_chemin;
            carte[x+1][y-2]=num_chemin;
            carte[x+1][y-3]=num_chemin;
            carte[x+1][y-4]=num_chemin;
            carte[x+2][y-4]=num_chemin;
          
            carte[x+3][y-4]=salle_arrive;

            x=x+3;
            y=y-4;
            
            position_salle[salle_arrive][0]=x;
            position_salle[salle_arrive][1]=y;
            break;
            
            //pattern 11  : Sud-Est tordu
            case "11":
            for(i=y+1; i<=y+2; i++){
                carte[x][i]=num_chemin;
            }
            i--;
            y=i;
            for(j=x+1; j<=x+3; j++){
                carte[j][i]=num_chemin;
            }
            j--;
            
            for(i=y+1; i<y+2; i++){
                carte[j][i]=num_chemin;
            }
            carte[j][i]=salle_arrive;
            position_salle[salle_arrive][0]=j;
            position_salle[salle_arrive][1]=i;
            break;
          
            //pattern 12  : aller a l'est
            case "12":
            for(i=x+1; i<x+6; i++){
                carte[i][y]=num_chemin;
            }
            carte[i][y]=salle_arrive;
            position_salle[salle_arrive][0]=i;
            position_salle[salle_arrive][1]=y;
            break;
          
            //pattern 13  : diago nord
            case "13":
            carte[x+1][y-1]=num_chemin;
            carte[x+2][y-2]=num_chemin;
            carte[x+3][y-3]=num_chemin;
            carte[x+3][y-4]=salle_arrive;
            x=x+3;
            y=y-4;
            position_salle[salle_arrive][0]=x;
            position_salle[salle_arrive][1]=y;
            break;
            
            //pattern 14  : nord est grand
            case "14":
            x=x+1;
            y=y-1;
            carte[x][y]=num_chemin;
            i=y-1;
            for(j=x+1; j<=x+5; j++){
                carte[j][i]=num_chemin;
            }
            j--;
            
            for(i=y-1; i>=y-2; i--){
              carte[j][i]=num_chemin;
            }

            carte[j][i]=salle_arrive;
            
            position_salle[salle_arrive][0]=j;
            position_salle[salle_arrive][1]=i;
            break;

            //pattern 15  : diago nord large
            case "15":
            carte[x+1][y-1]=num_chemin;
            carte[x+1][y-2]=num_chemin;
            carte[x+2][y-2]=num_chemin;
            carte[x+2][y-3]=num_chemin;
            carte[x+2][y-4]=num_chemin;
            
            carte[x+3][y-4]=salle_arrive;

            x=x+3;
            y=y-4;
            
            position_salle[salle_arrive][0]=x;
            position_salle[salle_arrive][1]=y;
            break;

            //pattern 16  : 4 cases en bas et 2 cases a droite
            case "16":
            for(i=y+1; i<=y+4; i++){
                carte[x+1][i]=num_chemin;
            }
            i--;
            for(j=x+1; j<=x+2; j++){
                carte[j][i]=num_chemin;
            }
            carte[j][i]=salle_arrive;
            
            position_salle[salle_arrive][0]=j;
            position_salle[salle_arrive][1]=i;
            break;
 
            case "17":
            carte[x+1][y+1]=num_chemin;
            carte[x+1][y+2]=num_chemin;
            carte[x+2][y+3]=num_chemin;
            
            carte[x+3][y+4]=salle_arrive;

            x=x+3;
            y=y+4;
            
            position_salle[salle_arrive][0]=x;
            position_salle[salle_arrive][1]=y;
            break;

            case "18":
            carte[x+1][y+1]=num_chemin;
            carte[x+1][y+2]=num_chemin;
            carte[x+1][y+3]=num_chemin;
            carte[x+1][y+4]=num_chemin;
            carte[x][y+4]=num_chemin;
            carte[x][y+5]=num_chemin;
            carte[x][y+6]=num_chemin;
            carte[x][y+7]=num_chemin;
                  
            carte[x][y+8]=salle_arrive;

            y=y+8;
            
            position_salle[salle_arrive][0]=x;
            position_salle[salle_arrive][1]=y;
            break;

            case "19":
            carte[x+1][y-1]=num_chemin;
            carte[x+2][y-2]=num_chemin;
            carte[x+2][y-3]=num_chemin;
          
            carte[x+3][y-4]=salle_arrive;

            x=x+3;
            y=y-4;
            
            position_salle[salle_arrive][0]=x;
            position_salle[salle_arrive][1]=y;
            break;

            default :
            //console.log("defaut");
            break;
          
        }
   
    
  }
  
}

function deplace_perso(){
    var perso = document.getElementById('perso');

    xp=parseInt(xp);
    yp=parseInt(yp);  
    
    xtmp=xp;
    ytmp=yp;

    //position de depart
    xmat=position_salle[salle_actuelle][0];
    ymat=position_salle[salle_actuelle][1];
    
    trouve_position_case();
    animation();
}

/**
 * !!!!!
 */
var ARRIVE=0;

function animation(){
    if(xp!=xtmp || yp!=ytmp){
        if(xp<xtmp){
            xp=xp+1;
        }else if(xp>xtmp){
            xp=xp-1;
        }
      
        if(yp<ytmp){
            yp=yp+1;
        }else if(yp>ytmp){
            yp=yp-1;
        }

        draw_fog();
        perso.style.top = yp+OFFSET_TOP+"px";
        perso.style.left = xp+OFFSET_LEFT+"px";
        setTimeout(animation, 20);
    }else if(ARRIVE){
        ARRIVE=0;
    }else{
        trouve_position_case();
      
        setTimeout(animation, 20);
        if(carte[xmat][ymat]==salle_arrive){
            ARRIVE=1;
            xmatmp=0;
            ymatmp=0;
            xmat=0;
            ymat=0;
        }
    }
  
}

function draw_fog() {
    var canvas=document.getElementById('canvas');
    var ctx = canvas.getContext("2d");
    
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.globalAlpha = 1;
    ctx.fillStyle = "rgba(0,0,0, 0.9)";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.globalCompositeOperation = "xor";
    ctx.beginPath();
    ctx.arc(xp+w_case/2, yp+h_case/2, radius, 0, Math.PI*2);
    ctx.fill();
    ctx.closePath();
}

//ATTENTION IL FAUT SAVE LA CASE D'AVANT POUR EVITER DE REVENENIR EN ARRIERE 
function trouve_position_case(){
  //recup salle debut, salle arrive et numchemin (choix)
    if((carte[xmat][ymat-1]==num_chemin || carte[xmat][ymat-1]==salle_arrive) && (xmatmp!=xmat || ymatmp!=ymat-1)){           //en haut
        xtmp=xtmp;
        ytmp=ytmp-h_case;

        xmatmp=xmat;
        ymatmp=ymat;
        
        xmat=xmat;
        ymat=ymat-1;
    }else if((carte[xmat+1][ymat]==num_chemin || carte[xmat+1][ymat]==salle_arrive)  && (xmatmp!=xmat+1 || ymatmp!=ymat)){     //a droite
        xtmp=xtmp+w_case;
        ytmp=ytmp;
        
        xmatmp=xmat;
        ymatmp=ymat;
        
        xmat=xmat+1;
        ymat=ymat;
    }else if((carte[xmat][ymat+1]==num_chemin || carte[xmat][ymat+1]==salle_arrive)  && (xmatmp!=xmat || ymatmp!=ymat+1)){       //en bas
        xtmp=xtmp;
        ytmp=ytmp+h_case;
        
        xmatmp=xmat;
        ymatmp=ymat;
        
        xmat=xmat;
        ymat=ymat+1;
    }else if((carte[xmat-1][ymat]==num_chemin || carte[xmat-1][ymat]==salle_arrive) && (xmatmp!=xmat-1 || ymatmp!=ymat)){       //a gauche
        xtmp=xtmp-w_case;
        ytmp=ytmp;
        
        xmatmp=xmat;
        ymatmp=ymat;
        
        xmat=xmat-1;
        ymat=ymat;
    }else if((carte[xmat+1][ymat-1]==num_chemin || carte[xmat+1][ymat-1]==salle_arrive) && (xmatmp!=xmat+1 || ymatmp!=ymat-1)){  //en haut a droite
        xtmp=xtmp+w_case;
        ytmp=ytmp-h_case;
        
        xmatmp=xmat;
        ymatmp=ymat;
        
        xmat=xmat+1;
        ymat=ymat-1;
    }else if((carte[xmat+1][ymat+1]==num_chemin || carte[xmat+1][ymat+1]==salle_arrive) && (xmatmp!=xmat+1 || ymatmp!=ymat+1 )){
        //en bas a droite
        xtmp=xtmp+w_case;
        ytmp=ytmp+h_case;
        
        xmatmp=xmat;
        ymatmp=ymat;
        
        xmat=xmat+1;
        ymat=ymat+1;
    }else if((carte[xmat-1][ymat+1]==num_chemin || carte[xmat-1][ymat+1]==salle_arrive) && (xmatmp!=xmat-1 || ymatmp!=ymat+1)){       //en bas a gauche
        xtmp=xtmp-w_case;
        ytmp=ytmp+h_case;
        
        xmatmp=xmat;
        ymatmp=ymat;
        
        xmat=xmat-1;
        ymat=ymat+1;
    }else if((carte[xmat-1][ymat-1]==num_chemin || carte[xmat-1][ymat-1]==salle_arrive) && (xmatmp!=xmat-1 || ymatmp!=ymat-1 )){       //en haut a gauche
        xtmp=xtmp-w_case;
        ytmp=ytmp-h_case;
        
        xmatmp=xmat;
        ymatmp=ymat;
        
        xmat=xmat-1;
        ymat=ymat-1;
    }
    xtmp=parseInt(xtmp);
    ytmp=parseInt(ytmp);
}

function StartTimer(){
    totalSeconds = tmp_attente;
    setInterval(Timer_Tick, 1000);
}

function Timer_Tick(){
    if (totalSeconds > 0) {
        totalSeconds--;
    }else{
        if(salle_actuelle == 44){
            document.location.href = "../php/fin.php";
        }
        /*
         * Il est possible de relancer l'affichage et l'envoi des requêtes ajax à cet endroit là pour correspondre avec le timer affiché à l'écran
         */
        totalSeconds = tmp_attente;
        sm--;
        lancement_jeu();
    }
}

function jouer_son(){
    audio = new Audio(son);
    audio.play();
}

/**
 * Deprecated
 */
function sauvegarde(){
    $.ajax({
        type: 'POST',
        url: '../php/sauvegarde.php',
        data: 'salle_actuelle='+salle_actuelle+'&pv='+pv+'&santeMental='+sm,
        success: function(data){
          console.log(data);
        },
        error: function(){
          alert('Erreur sauvegarde partie js');
        }
    });
}

/**
 * WIP -> Chargement
 */
function charger(){
  $.ajax({
      type: 'POST',
      url: '../php/charger.php',
      success: function(data){
        chargement=JSON.parse(data);
        pv = chargement[0].pv;
        sm = chargement[0].santeMental;
        salle_actuelle = chargement[0].fkSalle;

        //console.log(chargement);
        //console.log("histoire = "+chargement[0].image_name)
        //console.log("histoire = "+chargement[0]["histoire"])
        chargerImage("../../img/"+chargement[0].image_name);
        cleanText("");
        showText("#msg", chargement[0]["histoire"], 0, 10);

        xp=position_salle[salle_actuelle][0]*w_case;
        yp=position_salle[salle_actuelle][1]*h_case;
    
        perso.style.height=h_case+"px";
        perso.style.width=w_case+"px";
      
        perso.style.top = yp+OFFSET_TOP+"px";
        perso.style.left = xp+OFFSET_LEFT+"px";

        draw_fog();
        // alert("chargement partie reussie pv = "+pv+" sm = "+sm+" salle actuelle = "+salle_actuelle);
    },
    error: function(){
      alert('Erreur sauvegarde partie js');
    }
  });
}

