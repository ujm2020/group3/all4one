<?php
/*************************************************************/
/***                 Projet Tutoré L3-Info                 ***/ 
/***               Enregistrement des votes                ***/ 
/***           Clain-Januel-Laroche-Moulin-Verdun          ***/ 
/*************************************************************/
session_start();
    /**
    * Connexion à la bdd
    */
    $bdd = 'BD_groupe3';
    include_once('connexion.inc.php');
    $pdo = connex($bdd);
    /**
     * Fin connexion
     */

    //Recup du choix de l'utilisateur
    $vote = $_POST['choix'];
    $vote2 = $_POST['choix2'];
    $vote3 = $_POST['salle_actuelle'];
    $type = $_POST['type'];
    $vote_decode = intval(json_decode($vote));
    $vote_txt = json_decode($vote2);
    $vote_salle = intval(json_decode($vote3));
    $b_type = json_decode($type);
    /**
     * Zone requete en BDD
     */
    try{
        /**
         * Un choix:
         */
        if($b_type != 1){
            $req = $pdo->prepare('INSERT INTO reponse(valeur, fkSalle, fkChoix) VALUES(:txt, :salle, :vote)');
            $req->bindParam(':vote', $vote_decode, PDO::PARAM_INT);
            $req->bindParam(':txt', $vote_txt, PDO::PARAM_STR);
            $req->bindParam(':salle', $vote_salle, PDO::PARAM_INT);
            $req->execute();
        /**
         * Une action:
         */
        }else{
            $req = $pdo->prepare('INSERT INTO reponse(valeur, fkSalle, fkAction) VALUES(:txt, :salle, :vote)');
            $req->bindParam(':vote', $vote_decode, PDO::PARAM_INT);
            $req->bindParam(':txt', $vote_txt, PDO::PARAM_STR);
            $req->bindParam(':salle', $vote_salle, PDO::PARAM_INT);
            $req->execute();
        }
    }catch(PDOException $e){
        echo $e;
        die();
    }
?>
