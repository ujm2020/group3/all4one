<?php
/*****************************************************/
/***              Projet tutoré L3 info            ***/
/***              Affichage Score/pv/sm            ***/
/***  Clain-Januel-Laroche-Moulin-Verdun-Buzenet   ***/
/*****************************************************/
    session_start();
    include 'connexion.inc.php';
    $pdo=connex('BD_groupe3');

    $idPerso = $_POST['perso'];
    try{
        $req2 = $pdo->prepare("SELECT pv, santeMental FROM partie ORDER BY idPartie DESC LIMIT 1");
        $req2->execute();

        $value=$req2->fetchAll(PDO::FETCH_ASSOC);

        $req = $pdo->prepare("SELECT * FROM score WHERE fkPersonnalite=:pers");
        $req->bindParam(':pers', $idPerso, PDO::PARAM_INT);
        $req->execute();
        $val = $req->fetchAll(PDO::FETCH_ASSOC);

        $value[0]["score"] = $val[0][Score];

        $data = json_encode($value);
        echo $data;
        
    }catch(Exception $e){
        echo $e;
        die();
    }
?>