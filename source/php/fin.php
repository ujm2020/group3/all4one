<?php
/*************************************************************/
/***                 Projet Tutoré L3-Info                 ***/ 
/***                          FIN                          ***/ 
/***       Clain-Januel-Laroche-Moulin-Verdun-Buzenet      ***/ 
/*************************************************************/

    session_start();
    include 'connexion.inc.php';
    $pdo=connex('BD_groupe3');

    $req = $pdo->prepare("SELECT * FROM score ORDER BY Score DESC");
    $req->execute();
    $value=$req->fetchAll(PDO::FETCH_ASSOC);

    /* permier */
    $req = $pdo->prepare("SELECT * FROM personnalite WHERE idPerso=:perso");
    $req->bindParam(':perso', $value[0][fkPersonnalite], PDO::PARAM_INT);
    $req->execute();
    $un = $req->fetchAll(PDO::FETCH_ASSOC);
    /* deuxieme */
    $req = $pdo->prepare("SELECT * FROM personnalite WHERE idPerso=:perso");
    $req->bindParam(':perso', $value[1][fkPersonnalite], PDO::PARAM_INT);
    $req->execute();
    $deux = $req->fetchAll(PDO::FETCH_ASSOC);
    /* troisieme */
    $req = $pdo->prepare("SELECT * FROM personnalite WHERE idPerso=:perso");
    $req->bindParam(':perso', $value[2][fkPersonnalite], PDO::PARAM_INT);
    $req->execute();
    $trois = $req->fetchAll(PDO::FETCH_ASSOC);
    /* quatrieme */
    $req = $pdo->prepare("SELECT * FROM personnalite WHERE idPerso=:perso");
    $req->bindParam(':perso', $value[3][fkPersonnalite], PDO::PARAM_INT);
    $req->execute();
    $quatre = $req->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Fin</title>
        <link rel="stylesheet" type="text/css" href="../css/fin.css">
    </head>
    <body>

        <div class="fin">
            <?php
                if(isset($_SESSION['pseudo']) && strcmp($_SESSION['pseudo'], "admin") == 0){
                    $aff ='<h1> THE END </h1></br>';
                    $aff.='<h3 class="classement">Résultats</h3></br>';
                    $aff.='<p class="classement">1er : '.$un[0][label].', score : '.$value[0][Score].'</br>';
                    $aff.='<p class="classement">2e : '.$deux[0][label].', score : '.$value[1][Score].'</br>';
                    $aff.='<p class="classement">3e : '.$trois[0][label].', score : '.$value[2][Score].'</br>';
                    $aff.='<p class="classement">4e : '.$quatre[0][label].', score : '.$value[3][Score].'</p>';

                    echo $aff;
                }else{
                    $pas_co.= '<p >connecte vous</p>';
                    $pas_co.= '<a href="connexion.php"><input type="button" value="connexion"></a>';
                    $pas_co.= '<a href="inscription.php"><input type="button" value="inscription"></a>';
                    echo $pas_co;
                }
            ?>
        </div>
    </body>
</html>
