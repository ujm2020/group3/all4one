<?php
/*************************************************************/
/***                 Projet Tutoré L3-Info                 ***/ 
/***                 Suite du jeu -- Ecran                 ***/ 
/***           Clain-Januel-Laroche-Moulin-Verdun-Buzenet  ***/ 
/*************************************************************/
    session_start();
	/**
	* Connexion à la bdd
    */
    $bdd = 'BD_groupe3';
    include_once('connexion.inc.php');
    $pdo = connex($bdd);

    $salle=1;
    /**
     * Fin connexion
     */

 
    //Recup du choix de l'utilisateur
    $vote = $_POST['salle_actuelle'];
    $salle = intval(json_decode($vote));

    //Recup pv - sm + intval
    $pv_j = $_POST['pv'];
    $sm_j = $_POST['sm'];
    $pv = intval($pv_j);
    $sm = intval($sm_j);

    try{
        //Recup booléen action
        $req = $pdo->prepare('SELECT action FROM salle WHERE idSalle=:salle');
        $req->bindParam(':salle', $salle, PDO::PARAM_INT);
        $req->execute();
        $bool_value = $req->fetchAll(PDO::FETCH_ASSOC);
        $bool_action = $bool_value[0][action]; 
        
        
        //If sur choix/Action
        /**
         * Cas 1: Deplacement
         */
        if($bool_action == 0){
            $req = $pdo->prepare('SELECT COUNT(*) AS nbChoix, fkChoix FROM reponse JOIN choix ON reponse.fkChoix = choix.idChoix WHERE choix.fkSalle=:salle_actuelle GROUP BY fkChoix ORDER BY nbChoix DESC LIMIT 1');
            $req->bindParam(':salle_actuelle', $salle, PDO::PARAM_INT);
            $req->execute();
            
            $value=$req->fetchAll(PDO::FETCH_ASSOC);
            $idchoix=$value[0][fkChoix];

            $req = $pdo->prepare('SELECT * FROM partie ORDER BY idPartie DESC;');
            $req->execute();
            $value=$req->fetchAll(PDO::FETCH_ASSOC);
            $salle=$value[0][fkSalle];

            /*************************************************************/
            /***                    RECUP SCORE                        ***/ 
            /*************************************************************/
            
            $req = $pdo->prepare("SELECT idPerso FROM personnalite WHERE idPerso != 0");
            $req->execute();
            $pers = $req->fetchAll(PDO::FETCH_ASSOC);

            for($i=0;$i < count($pers);$i++){
                if(isset($pers[$i][idPerso])){
                    $req = $pdo->prepare("SELECT * FROM scoring WHERE fkChoix=:choix AND fkPersonnalite=:personnage");
                    $req->bindParam(':choix',$idchoix, PDO::PARAM_INT);
                    $req->bindParam(':personnage', $pers[$i][idPerso], PDO::PARAM_INT);
                    $req->execute();
                    $score = $req->fetchAll(PDO::FETCH_ASSOC);

                    $req = $pdo->prepare("SELECT * FROM score WHERE fkPersonnalite=:perso");
                    $req->bindParam(':perso', $pers[$i][idPerso], PDO::PARAM_INT);
                    $req->execute();
                    $stock_sc = $req->fetchAll(PDO::FETCH_ASSOC);

                    $sc = $stock_sc[0][Score];
                    $sc += $score[0][nbPoint];

                    $req = $pdo->prepare("UPDATE score SET Score=:sc WHERE fkPersonnalite=:perso");
                    $req->bindParam(':perso', $pers[$i][idPerso], PDO::PARAM_INT);
                    $req->bindParam(':sc', $sc, PDO::PARAM_INT);
                    $req->execute();
                }
            }
            
            /*************************************************************/
            /***                     END SCORE                         ***/ 
            /*************************************************************/

        /**
         * Cas 2: Action
         */
        }else{
            $req = $pdo->prepare('SELECT COUNT(*) AS nbAction, fkAction FROM reponse JOIN action ON reponse.fkAction = action.idAction WHERE action.fkSalle=:salle_actuelle GROUP BY fkAction ORDER BY nbAction DESC LIMIT 1');
            $req->bindParam(':salle_actuelle', $salle, PDO::PARAM_INT);
            $req->execute();
            
            $value=$req->fetchAll(PDO::FETCH_ASSOC);
            $idaction=$value[0][fkAction];

            $req = $pdo->prepare('SELECT * FROM partie ORDER BY idPartie DESC;');
            $req->execute();
            $value=$req->fetchAll(PDO::FETCH_ASSOC);
            $salle=$value[0][fkSalle];
            
            //Recup pv et sm depuis save
            $pv = $value[0][pv];
            $sm = $value[0][santeMental];

            $pv_save= $value[0][pv];
            $sm_save=$value[0][santeMental] ;

            /*************************************************************/
            /***                 Mise a jour pv-sm                     ***/ 
            /*************************************************************/
            //Recup idBonus
            $req = $pdo->prepare("SELECT idBonus FROM action WHERE idAction=:action");
            $req->bindParam(':action',$idaction, PDO::PARAM_INT);
            $req->execute();
            $a_bonus = $req->fetchAll(PDO::FETCH_ASSOC);
            
            /**
             * Many Action have One Bonus
             */
            $bonus = intval($a_bonus[0][idBonus]);
            
            $req = $pdo->prepare("SELECT COUNT(*) as 'force' FROM stuff WHERE arme=1");
            $req->execute();
            $a_buff = $req->fetchAll(PDO::FETCH_ASSOC);
            $buff = $a_buff[0][force];
            
            //recup sm et pv +/-
            $req = $pdo->prepare("SELECT * FROM bonus WHERE idBonus=:bonus");
            $req->bindParam(':bonus',$bonus, PDO::PARAM_INT);
            $req->execute();
            $a_bon = $req->fetchAll(PDO::FETCH_ASSOC);
            $bool_buff = $a_bon[0][bonus_score];

            //Gain de stats
            if($bool_buff == 1){
                if($pv<20){
                    $pv+=$a_bon[0][sante_phy];
                    if($pv==20){
                        $pv=20;
                    }
                }
                $sm+=$a_bon[0][sante_ment];
            //Perte de stats
            }else if($bool_buff==-1){
                //Perte de pv
                if(intval($a_bon[0][sante_phy])!=0){
                //Max perte, si perte max < 1 perte max =1
                    $max_perte_phy = intval($a_bon[0][sante_phy])-$buff;
                    if($max_perte_phy<1){
                        $max_perte_phy=1;
                    }
                    $rand_pv = rand(1, $max_perte_phy);
                    $pv-=$rand_pv;
                }
                //Perte SM
                if(intval($a_bon[0][sante_ment]) != 0){
                    $sm-=intval($a_bon[0][sante_ment]);
                }
            }
            
            /*************************************************************/
            /***                     END BONUS                         ***/ 
            /*************************************************************/

            /*************************************************************/
            /***                    RECUP SCORE                        ***/ 
            /*************************************************************/
            
            $req = $pdo->prepare("SELECT idPerso FROM personnalite");
            $req->execute();
            $pers = $req->fetchAll(PDO::FETCH_ASSOC);
            
            for($i=0;$i < count($pers);$i++){
                if(isset($pers[$i][idPerso])){
                    $req = $pdo->prepare("SELECT * FROM scoring_action WHERE fkAction=:action AND fkPersonnalite=:personnage");
                    $req->bindParam(':action',$idaction, PDO::PARAM_INT);
                    $req->bindParam(':personnage', $pers[$i][idPerso]);
                    $req->execute();
                    $score = $req->fetchAll(PDO::FETCH_ASSOC);
                    
                    $req = $pdo->prepare("SELECT * FROM score WHERE fkPersonnalite=:perso");
                    $req->bindParam(':perso', $pers[$i][idPerso], PDO::PARAM_INT);
                    $req->execute();
                    $stock_sc = $req->fetchAll(PDO::FETCH_ASSOC);

                    $sc = $stock_sc[0][Score];
                    $sc += $score[0][nbPoint];

                    $req = $pdo->prepare("UPDATE score SET Score=:sc WHERE fkPersonnalite=:perso");
                    $req->bindParam(':perso', $pers[$i][idPerso], PDO::PARAM_INT);
                    $req->bindParam(':sc', $sc, PDO::PARAM_INT);
                    $req->execute();
                }

            }
            
            /*************************************************************/
            /***                     END SCORE                         ***/ 
            /*************************************************************/


            /*************************************************************/
            /***                  Mise a jour du stuff                 ***/ 
            /*************************************************************/
            /* recupere les parametre de l'action effectuer */
            $req = $pdo->prepare("SELECT gain_perte, objet FROM action WHERE idAction = :idAction");
            $req->bindParam(':idAction',$idaction, PDO::PARAM_INT);
            $req->execute();
            $stuff=$req->fetchAll(PDO::FETCH_ASSOC);

            /* met a jout la table stuff en fonction */
            /* gagne un ogjet */
            if($stuff[0][gain_perte] == 1){
                $req = $pdo->prepare("INSERT INTO stuff(objet) VALUES (:objet)");
                $req->bindParam(':objet',$stuff[0][objet], PDO::PARAM_STR);
                $req->execute();
            }/* perte */
            else if($stuff[0][gain_perte] == 2){
                /* si il perd tous */
                if(strcmp($stuff[0][objet], 'deux') == 0){
                    $pdo->query("DELETE FROM stuff ORDER BY idStuff DESC LIMIT 2");
                }else if(strcmp($stuff[0][objet], 'tous') == 0){
                    $pdo->query("DELETE FROM stuff");
                }

            } /* achat */
            else if($stuff[0][gain_perte] == 3){
                $pdo->query("DELETE FROM stuff WHERE objet = 'bourse.png' LIMIT 1");
                /* si il achete un objet */
                if(strcmp($stuff[0][objet], 'bourse.png') != 0){
                    $req = $pdo->prepare("INSERT INTO stuff(objet) VALUES (:objet)");
                    $req->bindParam(':objet',$stuff[0][objet], PDO::PARAM_STR);
                    $req->execute();
                }
            }
            /*Recuperer une arme*/
            else if ($stuff[0][gain_perte] == 4){
                $req = $pdo->prepare("INSERT INTO stuff(objet,arme) VALUES (:objet,1)");
                $req->bindParam(':objet',$stuff[0][objet], PDO::PARAM_STR);
                $req->execute(); 
            }
            /*************************************************************/
            /***                       End stuff                       ***/ 
            /*************************************************************/
        }


        /**
         * reset des reponses
         */
        /**
         * Call --> SQL Procedure --> reset reponse 
         */
        $req = $pdo->prepare('CALL reset_reponse()');
        $req->execute();
        

        $req = $pdo->prepare('SELECT * FROM salle WHERE idSalle=:salle;');
        $req->bindParam(':salle', $salle, PDO::PARAM_INT);
        $req->execute();
        $value=$req->fetchAll(PDO::FETCH_ASSOC);

        if($value[0][action] == 1){
            /**
             * Récup salle + Check si action disponible
             */
            $req = $pdo->prepare('SELECT idAction, texte, label,fkSalle FROM action JOIN salle ON salle.idSalle=action.fkSalle WHERE action.idAction=:idaction LIMIT 1');
            $req->bindParam(':idaction', $idaction, PDO::PARAM_INT);
            $req->execute();

            $val = $req->fetchAll(PDO::FETCH_ASSOC);

            //Reset action pour suite jeu
            $req2 = $pdo->prepare('UPDATE salle SET action=0 WHERE idSalle=:action');
            $req2->bindParam(':action', $val[0][fkSalle], PDO::PARAM_INT);
            $req2->execute();

            $req = $pdo->prepare('SELECT histoire, image_name ,son FROM salle WHERE idSalle=:salle');
            $req->bindParam(':salle', $val[0][fkSalle], PDO::PARAM_INT);
            $req->execute();
            $s_actu = $val[0][fkSalle];


        }else{
            /**
             * Recup choix si pas/plus d'action
             */
            $req = $pdo->prepare('SELECT fkSalleSuiv, label, numChemin, idSalle, histoire, image_name, son FROM choix JOIN salle ON salle.idSalle=choix.fkSalle WHERE choix.idchoix=:idchoix LIMIT 1');
            $req->bindParam(':idchoix', $idchoix, PDO::PARAM_INT);
            $req->execute();
            $val = $req->fetchAll(PDO::FETCH_ASSOC);

            $req = $pdo->prepare('SELECT histoire, image_name ,son, visited FROM salle WHERE idSalle=:salle');
            $req->bindParam(':salle', $val[0][fkSalleSuiv], PDO::PARAM_INT);
            $req->execute();
            $s_actu = $val[0][fkSalleSuiv];  
        
            //savoir si on est passe dans la salle
            $req2 = $pdo->prepare('UPDATE salle SET visited=1 WHERE idSalle=:salle');
            $req2->bindParam(':salle', $val[0][idSalle], PDO::PARAM_INT);
            $req2->execute();
                
        }
        //On recup l'histoire
        $histoire = $req->fetchAll(PDO::FETCH_ASSOC);
            
        
        //Gestion fichier
        if($value[0][action] == 1){
            $filename = "../../txt/salles/".$val[0][texte]."";
            $file= fopen($filename,"r");
            $contents = fread($file, filesize($filename));
            fclose($file);
        }else if($value[0][action] == 0 && $histoire[0][visited] == 1){
            $filename = "../../txt/salles/visited.txt";
            $file= fopen($filename,"r");
            $contents = fread($file, filesize($filename));
            fclose($file);
        }else{
            $filename = "../../txt/salles/".$histoire[0][histoire]."";
            $file= fopen($filename,"r");
            $contents = fread($file, filesize($filename));
            fclose($file);
        }
        
        //Recup son + img, invariables
        $val[0][son]="../../son/".$histoire[0][son]."";
        $val[0][image_name]="../../img/".$histoire[0][image_name]."";
        $val[0]["action"] = $value[0][action];
        $val[0]["pv"] = intval($pv);
        $val[0]["sm"] = intval($sm);

        //Insertion données dans tableau
        $val[0][histoire] = $contents;

        //Preparation save
        $req = $pdo->prepare("SELECT * FROM partie ORDER BY idPartie DESC LIMIT 1");
        $req->execute();
        /* recuperation de la derniere partie */
        $value=$req->fetchAll(PDO::FETCH_ASSOC);
        $idPartie = $value[0][idPartie];

        /**
         * Il manque sm et pv comme var
         */
        $save=$pdo->prepare("UPDATE partie SET pv=:pv, santeMental=:sm, fkSalle=:salle, temps=current_timestamp WHERE idPartie=:id");
        $save->bindParam(':pv',$pv, PDO::PARAM_INT);
        $save->bindParam(':sm',$sm, PDO::PARAM_INT);
        $save->bindParam(':salle',$s_actu, PDO::PARAM_INT);
        $save->bindParam(':id',$idPartie, PDO::PARAM_INT);
        $save->execute();


        $data = json_encode($val);
        echo $data;
           
    }catch(Exception $e){
        echo $e;
        die();
    }

?>
