
<?php
/*****************************************************/
  /***         Projet tutoré L3 info               ***/
  /***           Affichage Smartphone              ***/
  /***  Clain-Januel-Laroche-Moulin-Verdun-Buzenet ***/
  /***************************************************/

    session_start();
    include 'connexion.inc.php';
    $pdo=connex('BD_groupe3');

    try{
        /* on regarde si la partie est lance */
        $req = $pdo->prepare("SELECT idPerso, label, image FROM personnalite JOIN utilisateur ON personnalite.idPerso=utilisateur.fkPersonnalite WHERE utilisateur.pseudo = :pseudo");
        $req->bindParam(":pseudo", $_SESSION["pseudo"], PDO::PARAM_STR);
        $req->execute();

        $value=$req->fetchAll(PDO::FETCH_ASSOC);
        $idPerso = $value[0][idPerso];
        $nom_perso = $value[0][label];
        $img_perso = $value[0][image];



    }catch(Exception $e){
        echo $e;
        die();
    }

?>

<!DOCTYPE HTML>
<html>
    <?php
        if(isset($_SESSION['pseudo'])){
    ?>
    <head>
        <title>Smartphone</title>
        <meta charset ="utf-8"/>
        <link rel="stylesheet" href="../css/smartphone.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="../js/gerer_bouton.js"></script>
    </head>
    <body id="body">
        <div id="cache">
            <p id="tourner">Tourner le smartphone</p>
        </div>
        <div id="sac">
            <p id="inventaire">Inventaire</p>
            <table>
                <tr>
                    <td id="td0"></td>
                    <td id="td1"></td>
                    <td id="td2"></td>
                    <td id="td3"></td>
                </tr>
                <tr>
                    <td id="td4"></td>
                    <td id="td5"></td>
                    <td id="td6"></td>
                    <td id="td7"></td>
                </tr>
                <tr>
                    <td id="td8"></td>
                    <td id="td9"></td>
                    <td id="td10"></td>
                    <td id="td11"></td>
                </tr>
                <tr>
                    <td id="td12"></td>
                    <td id="td13"></td>
                    <td id="td14"></td>
                    <td id="td15"></td>
                </tr>
            </table>	
        </div>
        <div id="Interface">
            <?php
                echo '<div id="personnalite"><img id="img_perso" src="../../img/'.$img_perso.'" alt="Hache"><p id="nom_perso" title="'.$idPerso.'">'.$nom_perso.'</p><p id="score"></p><p id="pv"></p><p id="sm"></p></div>'
            ?>
            <p id="Timer"></p>
            <p id='le_p'> </p>
        </div>
    </body>
    <?php
        }else {
    ?>
        <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Menu</title>
        <link rel="stylesheet" type="text/css" href="../css/debut.css">
        </head>
        <body>
        <?php
        $pas_co = '<div class="deja_co">';
        $pas_co.= '<p>connecte vous</p>';
        $pas_co.= '<a href="connexion.php"><input type="button" value="connexion"></a>';
        $pas_co.= '<a href="inscription.php"><input type="button" value="inscription"></a>';
        $pas_co.= '</div>';
        echo $pas_co;
        ?>
      </body>
    <?php
        }
    ?>
</html>
