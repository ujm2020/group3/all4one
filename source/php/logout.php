<?php
/*****************************************************/
/***              Projet tutoré L3 info            ***/
/***                   Deconnexion                 ***/
/***  Clain-Januel-Laroche-Moulin-Verdun-Buzenet   ***/
/*****************************************************/
    session_start();
    session_unset();
    session_destroy();

    header('Location:connexion.php');

?>