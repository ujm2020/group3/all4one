<?php
/*************************************************************/
/***                 Projet Tutoré L3-Info                 ***/ 
/***                        FIN-Tel                        ***/ 
/***       Clain-Januel-Laroche-Moulin-Verdun-Buzenet      ***/ 
/*************************************************************/


    session_start();
    include 'connexion.inc.php';
    $pdo=connex('BD_groupe3');

    /* recupere tous les scores */
    $req = $pdo->prepare("SELECT * FROM score ORDER BY score DESC");
    $req->execute();
    $score=$req->fetchAll(PDO::FETCH_ASSOC);

    /* recupere la personnalite */
    $req = $pdo->prepare("SELECT * FROM utilisateur WHERE pseudo=:pseudo");
    $req->bindParam(':pseudo', $_SESSION['pseudo'], PDO::PARAM_STR);
    $req->execute();
    $perso=$req->fetchAll(PDO::FETCH_ASSOC);

    /* recupere le score du joueur */
    $req = $pdo->prepare("SELECT * FROM score WHERE fkPersonnalite=:perso");
    $req->bindParam(':perso', $perso[0][fkPersonnalite], PDO::PARAM_STR);
    $req->execute();
    $son_score=$req->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Attente</title>
        <link rel="stylesheet" type="text/css" href="../css/debut.css">
    </head>
    <body>

        <div class="deja_co">
            <?php
                if(isset($_SESSION['pseudo'])){
                    $aff ='<h1> THE END </h1></br>';
                    if(strcmp($score[0][fkPersonnalite], $perso[0][fkPersonnalite]) == 0){
                        $aff.='<h3>Victoire !!!</h3></br>';
                    }else{
                        $aff.='<h3>Défaite</h3></br>';
                    }
                    $aff.='votre score : '.$son_score[0][Score].'</p>';

                    echo $aff;
                }else{
                    $pas_co.= '<p >connecte vous</p>';
                    $pas_co.= '<a href="connexion.php"><input type="button" value="connexion"></a>';
                    $pas_co.= '<a href="inscription.php"><input type="button" value="inscription"></a>';
                    echo $pas_co;
                }
            ?>
        </div>
    </body>
</html>