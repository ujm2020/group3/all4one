<?php
/*****************************************************/
/***              Projet tutoré L3 info            ***/
/***                 Paramétrage BDD               ***/
/***  Clain-Januel-Laroche-Moulin-Verdun-Buzenet   ***/
/*****************************************************/
    function connex($base){
        include_once("param.inc.php");
        try{
            $pdo = new PDO('mysql:host='.HOTE.';dbname='.$base.';charset=utf8', UTILISATEUR, PASSE);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            echo 'Problème à la connexion';
            die();
        }

        return $pdo;
    }
?>
