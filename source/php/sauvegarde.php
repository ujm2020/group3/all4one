<?php
/*************************************************************/
/***                 Projet Tutoré L3-Info                 ***/ 
/***               Suite du jeu -- Téléphone               ***/ 
/***           Clain-Januel-Laroche-Moulin-Verdun-Buzenet  ***/ 
/*************************************************************/

    $bdd = 'BD_groupe3';
    include_once('connexion.inc.php');
    $pdo = connex($bdd);

    $salle = $_POST['salle_actuelle'];
    $pv = $_POST['pv'];
    $santeMental  = $_POST['santeMental'];

    $salle = intval(json_decode($salle));
    $pv = intval(json_decode($pv));
    $santeMental = intval(json_decode($santeMental));

    $date = date("Y-m-d h:i:sa");

    try{
        /* selectionne la derniere partie */
        $req = $pdo->prepare("SELECT * FROM partie ORDER BY idPartie DESC LIMIT 1");
        $req->execute();
        /* recuperation de la derniere partie */
        $value=$req->fetchAll(PDO::FETCH_ASSOC);
        $idPartie = $value[0][idPartie];

        $save=$pdo->prepare("UPDATE partie SET pv=:pv, santeMental=:sm, fkSalle=:salle, temps=current_timestamp WHERE idPartie=:id");
        $save->bindParam(':pv',$pv, PDO::PARAM_INT);
        $save->bindParam(':sm',$santeMental, PDO::PARAM_INT);
        $save->bindParam(':salle',$salle, PDO::PARAM_INT);
        $save->bindParam(':id',$idPartie, PDO::PARAM_INT);
        $save->execute();
         
        echo $idPartie;

    }catch(Exception $e){
        echo $e;
        die();
    }
?>
