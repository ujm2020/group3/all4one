<?php
/*****************************************************/
/***              Projet tutoré L3 info            ***/
/***                      Carte                    ***/
/***  Clain-Januel-Laroche-Moulin-Verdun-Buzenet   ***/
/*****************************************************/
    session_start();
    include 'connexion.inc.php';
    $pdo=connex('BD_groupe3');

    if(isset($_SESSION['pseudo'])){
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>CARTE</title>
        <link rel="stylesheet" type="text/css" href="../css/carte.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="../js/gerer_carte.js"></script>
    </head>

    <body>
        <div id="barre">
            <div class="boussole">
                <img id="boussole" src="../../img/boussole.png" height="200" width="200"/>
            </div>
            <div class="cadreImage">
                <img id="cadreImage"/>
            </div>
            
            <div class="boite" id="msg">
                <br/>
            </div>
        </div>
        
        <div class="fond"></div>
        <div class="carte">
            <img id="carte" src="../../img/Map.png" height="840" width="1870"/>
            <canvas id="canvas" width="1870" height="840">
            </canvas>
        </div>  
      
        <img id="perso" src="../../img/pers.gif" height="20" width="20"/> 
    </body>
  
</html>

<?php
    }else{
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>CARTE</title>
        <link rel="stylesheet" type="text/css" href="../css/debut.css" />
    </head>

    <body>
        <?php
            $pas_co = '<div class="deja_co">';
            $pas_co.= '<p>connecte vous</p>';
            $pas_co.= '<a href="connexion.php"><input type="button" value="connexion"></a>';
            $pas_co.= '<a href="inscription.php"><input type="button" value="inscription"></a>';
            $pas_co.= '</div>';
            echo $pas_co;
        ?>
    </body>
</html>
<?php
    }
?>
