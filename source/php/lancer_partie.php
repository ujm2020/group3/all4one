<?php
/*************************************************************/
/***                 Projet Tutoré L3-Info                 ***/ 
/***                  Lancement de partie                  ***/ 
/***       Clain-Januel-Laroche-Moulin-Verdun-Buzenet      ***/ 
/*************************************************************/
/* selectionne la derniere partie et met la colone 'lance' a 1 pour lancer le jeu cote smartphone */

    session_start();
    include 'connexion.inc.php';
    $pdo=connex('BD_groupe3');

    $status_partie = 1;

    $req = $pdo->prepare("SELECT * FROM partie ORDER BY idPartie DESC LIMIT 1");
    $req->execute();
    $value=$req->fetchAll(PDO::FETCH_ASSOC);
    $idPartie = $value[0][idPartie];

    $lancer_partie=$pdo->prepare("UPDATE partie SET lance=:lance WHERE idPartie=:id");
    $lancer_partie->bindParam(':lance',$status_partie, PDO::PARAM_INT);
    $lancer_partie->bindParam(':id',$idPartie, PDO::PARAM_INT);
    $lancer_partie->execute();

    header('location: carte.php');
?>
