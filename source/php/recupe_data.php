<?php
/*************************************************************/
/***                 Projet Tutoré L3-Info                 ***/ 
/***                   Recuperatio données                 ***/ 
/***           Clain-Januel-Laroche-Moulin-Verdun-Buzenet  ***/ 
/*************************************************************/

    $bdd = 'BD_groupe3';
	include_once('connexion.inc.php');
	$pdo = connex($bdd);

    /* tabelau des donnees a renvoyer */
    $data = array();

    try{
        $req = $pdo->prepare("SELECT * FROM partie ORDER BY idPartie DESC LIMIT 1");
        $req->execute();
        /* recuperation du temps bdd */
        $value=$req->fetchAll(PDO::FETCH_ASSOC);
        
        /* calcule des donnee de a partie en cours */
        $data[0] = $value[0][fkSalle];
        $chrono = time() - strtotime($value[0][temps]);
        $chrono = 35-$chrono;
        $data[1] = $chrono;

        if($data[0] == 1){
            $data[0] = 0;
        }
        $retour = json_encode($data);
        echo $retour;

    }catch(Exception $e){
        echo $e;
        die();
    }
?>
