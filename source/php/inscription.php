<?php
/*************************************************************/
/***                 Projet Tutoré L3-Info                 ***/ 
/***                      Inscription                      ***/ 
/***       Clain-Januel-Laroche-Moulin-Verdun-Buzenet      ***/ 
/*************************************************************/
    session_start();
    include 'connexion.inc.php';
    $pdo=connex('BD_groupe3');
    
    function afficheFormulaire($p){
        $c="<form action=".$_SERVER['PHP_SELF']." method=\"post\" class=\"connexion\">";
        $c.="<legend>Inscription</legend>";
        if(isset($p)){
            $c.='<p class="erreur_co">'.$p.'<p>';
            $c.="<div class=\"global_enter2\">";
        }else{
            $c.="<div class=\"global_enter\">";
        }
        $c.="<div class=\"enter\"><input id=\"pseudo\" placeholder=\"Pseudo\" type=\"text\" name=\"pseudo\"></div>";
        $c.="<div class=\"enter\"><input id=\"mdp\" placeholder=\"Mot de passe\" type=\"password\" name=\"pass\"></div>";
        $c.="<div class=\"enter\" id=\"input\"><input type=\"submit\" value=\"Inscription\" />";
        $c.='<a href="connexion.php"><input type="button" value="page connection"></a></div></div>';
        $c.="</form>";
        echo $c;
    }
  
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Inscription</title>
        <link rel="stylesheet" type="text/css" href="../css/debut.css">
    </head>
    <body>
        <?php
            if(isset($_SESSION['pseudo'])){
                $deja_co = '<div class="deja_co">';
                $deja_co.= '<p>Vous etes deja connecte</p>';
                if($_SESSION['status'] == 1){
                    $deja_co.= '<a href="menu.php"><input type="button" value="menu"></a>';
                }else{
                    $deja_co.= '<a href="attente.php"><input type="button" value="page attente"></a>';
                }
                $deja_co.= '<a href="logout.php"><input type="button" value="deconnexion"></a>';
                $deja_co.= '</div>';
                echo $deja_co;
            }else{
              /* on verifi que les champs ne sont pas vide et on enleve les espaces inutile */
                if(isset($_POST['pseudo']) && isset($_POST['pass']) && !empty($_POST['pseudo']) && !empty($_POST['pass'])){
                    $ok=1;
                    $ps=trim($_POST['pseudo']);
                    $mdp=trim($_POST['pass']);
                    $test_ins="SELECT * FROM utilisateur";
                    foreach($pdo->query($test_ins) as $row){
                        // Avec le foreach je vérifie si le pseudo est déja existant dans la BDD
                        if(strcmp($row['pseudo'],$ps)==0){
                            $ok=0;
                        }
                        
                    }
                    if($ok==0){
                        afficheFormulaire("pseudo déja utilisé");
                    }else{
                        // Je prépare la requette pour insérer un élément dans la base de donnée
                        //personnalite a O au debut
                        $perso = 0;
                        $inscr=$pdo->prepare("INSERT INTO utilisateur (pseudo, password, fkPersonnalite)VALUES (:pseudo,:password,:personnalite)");
                        $inscr->bindParam(':pseudo',$ps);
                        $inscr->bindParam(':password',md5($mdp));
                        $inscr->bindParam(':personnalite',$perso);
                        $inscr->execute();

                        /* connexion */
                        $_SESSION['pseudo']=$ps;
                        /* une fois connecte on redirige l'utilisateur vers une page d'attente des autres joueurs */
                        header('location: attente.php');
                    }
                }
                else{
                    afficheFormulaire(null);
                }
            }
        ?>
    </body>
</html>
