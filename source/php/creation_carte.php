<?php
/*************************************************************/
/***                 Projet Tutoré L3-Info                 ***/ 
/***                   Création de carte                   ***/ 
/***       Clain-Januel-Laroche-Moulin-Verdun-Buzenet      ***/ 
/*************************************************************/
    session_start();
	/**
	* Connexion à la bdd
    */
    
	$bdd = 'BD_groupe3';
	include_once('connexion.inc.php');
	$pdo = connex($bdd);
	/**
     * Fin connexion
     */

    /**
     * Zone requete en BDD
     */
    try{
        $req = $pdo->prepare('SELECT * FROM choix WHERE numChemin !=0 ORDER BY fksalle');
        $req->execute();

        $val = $req->fetchAll(PDO::FETCH_ASSOC);

        $data = json_encode($val);
        echo $data;
    }catch(Exception $e){
        echo $e;
        die();
    }

?>
