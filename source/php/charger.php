<?php
/*************************************************************/
/***                 Projet Tutoré L3-Info                 ***/ 
/***                       Chargement                      ***/ 
/***           Clain-Januel-Laroche-Moulin-Verdun-Buzenet  ***/ 
/*************************************************************/

	$bdd = 'BD_groupe3';
	include_once('connexion.inc.php');
	$pdo = connex($bdd);

    try{
        $req = $pdo->prepare("SELECT * FROM partie ORDER BY idPartie DESC LIMIT 1");
        $req->execute();
        /* recuperation de la derniere partie */
        $value=$req->fetchAll(PDO::FETCH_ASSOC);

        $req = $pdo->prepare('SELECT histoire, image_name FROM salle WHERE idSalle=:salle');
        $req->bindParam(':salle', $value[0][fkSalle], PDO::PARAM_INT);
        $req->execute();
        $value2=$req->fetchAll(PDO::FETCH_ASSOC);

        $value[0]["image_name"] = $value2[0][image_name];

        $filename = "../../txt/salles/".$value2[0][histoire]."";
        $file= fopen($filename,"r");
        $contents = fread($file, filesize($filename));
        fclose($file);
        $value[0]["histoire"] = $contents;

        $partie = json_encode($value);
        echo $partie;
    }catch(Exception $e){
        echo $e;
        die();
    }
?>
