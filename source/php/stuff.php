<?php
/*************************************************************/
/***                 Projet Tutoré L3-Info                 ***/ 
/***                        Stuff                          ***/ 
/***           Clain-Januel-Laroche-Moulin-Verdun-Buzenet  ***/ 
/*************************************************************/

    $bdd = 'BD_groupe3';
    include_once('connexion.inc.php');
    $pdo = connex($bdd);

    try{
        /* envoie du stuff */
        $req = $pdo->query("SELECT objet FROM stuff");
        $value=$req->fetchAll(PDO::FETCH_ASSOC);
        $data = json_encode($value);
        echo $data;

    }catch(Exception $e){
        echo $e;
        die();
    }
?>
