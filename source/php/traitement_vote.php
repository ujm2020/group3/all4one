<?php
/*************************************************************/
/***                 Projet Tutoré L3-Info                 ***/
/***                 Traitement des votes                  ***/
/***                       Deprecated                      ***/
/***           Clain-Januel-Laroche-Moulin-Verdun          ***/
/*************************************************************/
    session_start();
	/**
	* Connexion à la bdd
    */
	$bdd = 'BD_groupe3';
	include_once('connexion.inc.php');
	$pdo = connex($bdd);
	/**
     * Fin connexion
     */


    //Recup du choix de l'utilisateur
    /**
     * Zone requete en BDD
     */
    try{
        $val = $pdo->prepare('SELECT idReponse, MAX(mycount) FROM (SELECT idReponse, COUNT(valeur) mycount FROM reponse GROUP BY valeur)');
        $val->execute();
        $nb = $val->rowCount();
        $row = $val -> fetch(PDO::FETCH_ASSOC);
        $choix = $row['idReponse'];

        $rq = $pdo->prepare('DELETE FROM reponse');
        $rq->execute();
        
        //Requete bdd
        $req=$pdo->prepare('SELECT * FROM salle WHERE id_salle = 
                            (SELECT fk_salle FROM choix WHERE id_choix = :choix)
                          ');
        $req->bindParam(':choix', $choix, PDO::PARAM_INT);
        $req->execute();

        //stockage salle actuelle
        $row = $req -> fetch(PDO::FETCH_ASSOC);
        if ($nb == 1){
            $salle = $row['id_salle'];
        }
        //Requete bdd -> Recup choix
        $req=$pdo->prepare('SELECT * FROM choix WHERE id_salle = :salle'); 
        $req->bindParam(':salle', $salle["id_salle"], PDO::PARAM_INT);
        $req->execute();
        //stockage choix liés à la salle
        $i=0;
        while($row = $req -> fetch(PDO::FETCH_ASSOC)){   
            $choix[$i] = $row;
            $i++;
        }


        $score = calcul_score();
        /**
         * Renvoi des données
         */

        //....

    }catch(PDOException $e){
        echo $e;
        die();
    }

?>
