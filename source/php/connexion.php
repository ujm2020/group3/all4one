<?php
/*****************************************************/
/***              Projet tutoré L3 info            ***/
/***                    Connexion                  ***/
/***  Clain-Januel-Laroche-Moulin-Verdun-Buzenet   ***/
/*****************************************************/
    session_start();
    include 'connexion.inc.php';
    $pdo=connex('BD_groupe3');
    
    function afficheFormulaire($p){
        $c="<form action=".$_SERVER['PHP_SELF']." method=\"post\" class=\"connexion\">";
        $c.="<legend>Connexion</legend>";
        if(isset($p)){
            $c.='<p class="erreur_co">'.$p.'<p>';
            $c.="<div class=\"global_enter2\">";
        }
        else{
            $c.="<div class=\"global_enter\">";
        }
        $c.="<div class=\"enter\"><input id=\"pseudo\" placeholder=\"Pseudo\" type=\"text\" name=\"pseudo\"></div>";
        $c.="<div class=\"enter\"><input id=\"mdp\" placeholder=\"Mot de passe\" type=\"password\" name=\"pass\"></div>";
        $c.="<div class=\"enter\" id=\"input\"><input type=\"submit\" value=\"Connexion\" />";
        $c.='<a href="inscription.php"><input type="button" value="Page inscription"></a></div></div>';
        $c.="</form>";
        echo $c;
    }
  
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Connexion</title>
        <link rel="stylesheet" type="text/css" href="../css/debut.css">
    </head>
    <body>
        <?php
            if(isset($_SESSION['pseudo'])){
                $deja_co = '<div class="deja_co">';
                $deja_co.= '<p>Vous etes deja connecte</p>';
                if($_SESSION['status'] == 1){
                  $deja_co.= '<a href="menu.php"><input type="button" value="menu"></a>';
                }
                else{
                  $deja_co.= '<a href="attente.php"><input type="button" value="page attente"></a>';
                }
                $deja_co.= '<a href="logout.php"><input type="button" value="deconnexion"></a>';
                $deja_co.= '</div>';
                echo $deja_co;
            }else{
                /* on verifie que les champs ne sont pas vide et on enleve les espaces inutile */
                if(isset($_POST['pseudo']) && isset($_POST['pass']) && !empty($_POST['pseudo']) && !empty($_POST['pass'])){
                    $ok=1;
                    $ps=trim($_POST['pseudo']);
                    $mdp=trim($_POST['pass']);
                    $test_co="SELECT * FROM utilisateur ";
                    foreach($pdo->query($test_co) as $row){
                        if(strcmp($row['pseudo'],$ps)==0){
                            if(strcmp($row['password'],md5($mdp))==0){
                                $ok=0;
                                $_SESSION['pseudo']=$ps;
                                $_SESSION['status'] = $row['status'];
                                if($_SESSION['status'] == 1){
                                    header('location: menu.php');
                                }
                                else{
                                    header('location: attente.php');
                                }
                            }
                        }
                    }
                }
                if($ok==1){
                    afficheFormulaire("Erreur de pseudo ou de mot de passe"); 
                } 
                else{
                    afficheFormulaire(null);
                }
            }
        ?>
    </body>
</html>
