<?php
/*****************************************************/
/***              Projet tutoré L3 info            ***/
/***                     Attente                   ***/
/***  Clain-Januel-Laroche-Moulin-Verdun-Buzenet   ***/
/*****************************************************/

    session_start();
    include 'connexion.inc.php';
    $pdo=connex('BD_groupe3');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Attente</title>
        <link rel="stylesheet" type="text/css" href="../css/debut.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="../js/attente.js"></script>
    </head>
    <body>

        <div class="deja_co">
            <?php
                if(isset($_SESSION['pseudo'])){
                    $aff ='Attente du lancement de la parti.</br> Tourner votre smartphone une fois la partie lancé.<hr>';
                    $aff.='<p class="info">'.$_SESSION['pseudo'].' vous êtes l\'une des nombreuses personnalités de Krane An Katr.</br>';
                    $aff.='Vous jouez tous le même personnage. Chaque joueur aura une des personnalités.';
                    $aff.=' Faites les bon choix pour que votre personnalité triomphe.</br></p>';
                    $aff.='<hr><p class="info">Combattant : Vous avez soif de combat et de sang.</br></p>';
                    $aff.='<hr><p class="info">Pacifiste : Vous êtes considéré comme un trouillard qui évite les combats mais vous savez que votre cause est juste.</br></p>';
                    $aff.='<hr><p class="info">Collectionneur : Jour et nuit les richesses de ce monde vous obsèdent, vous les voulez toutes... absolument toutes.</br></p>';
                    $aff.='<hr><p class="info">Explorateur : Le ciel est la limite tout le reste n\'attend que votre passage.</p>';

                    echo $aff;

                }else{
                  $pas_co.= '<p >connecte vous</p>';
                  $pas_co.= '<a href="connexion.php"><input type="button" value="connexion"></a>';
                  $pas_co.= '<a href="inscription.php"><input type="button" value="inscription"></a>';
                  echo $pas_co;
                }
            ?>
        </div>
    </body>
</html>
