<?php
/*************************************************************/
/***                 Projet Tutoré L3-Info                 ***/ 
/***                   Création de partie                  ***/ 
/***       Clain-Januel-Laroche-Moulin-Verdun-Buzenet      ***/ 
/*************************************************************/
    session_start();
    include 'connexion.inc.php';
    $pdo=connex('BD_groupe3');

    $date = date('Y-m-d H:i:s');

    $new_game=$pdo->prepare("INSERT INTO partie(temps) VALUES(current_timestamp)");
    $new_game->execute();

    $req = $pdo->prepare('SELECT idPerso From personnalite');
    $req->execute();
    $pers=$req->fetchAll(PDO::FETCH_ASSOC);

    /**
     * reset des actions
     */
    /**
     * Call --> SQL Procedure --> reset_action
     */
    $req  = $pdo->prepare('CALL reset_action()');
    $req->execute();

    /* Vider stuff */
    $pdo->query("DELETE FROM stuff");
    $pdo->query("ALTER TABLE stuff AUTO_INCREMENT = 1");
    $pdo->query("INSERT INTO stuff (objet) VALUES ('bourse.png'), ('bourse.png'), ('bourse.png')");
    $req  = $pdo->prepare('CALL reset_visited()');
    $req->execute();

    header('location: lancer_partie.php');
?>
