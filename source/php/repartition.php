<?php
    $bdd = 'BD_groupe3';
    include_once('connexion.inc.php');
    $pdo = connex($bdd);

    /* Récuperation de tous les utilisateurs */
    $req = $pdo->prepare("SELECT * FROM utilisateur");
    $req->execute();
    $nb_utilisateur = $req->rowCount();
    $utilisateurs = $req->fetchAll(PDO::FETCH_COLUMN,0);

    /* Recuperation des personnalites */
    $req2 = $pdo->prepare("SELECT * FROM personnalite WHERE idPerso!=0");
    $req2->execute();
    $nb_personnalite = $req2->rowCount();
    $personnalite = $req2->fetchAll(PDO::FETCH_COLUMN,0);

    /* Melange des personnalites et on en recupere 4 */
    shuffle($personnalite);
    $p1 = $personnalite[0];
    $p2 = $personnalite[1];
    $p3 = $personnalite[2];
    $p4 = $personnalite[3];

    /*Initialisation de la table des scores*/
    
    $pdo->query("DELETE FROM score");

    $pdo->query("INSERT INTO score (fkPersonnalite, Score) VALUES ($p1,0)");
    $pdo->query("INSERT INTO score (fkPersonnalite, Score) VALUES ($p2,0)");
    $pdo->query("INSERT INTO score (fkPersonnalite, Score) VALUES ($p3,0)");
    $pdo->query("INSERT INTO score (fkPersonnalite, Score) VALUES ($p4,0)");


    /* Si pas moin de 4 utilisateurs on attribut les personnalité tels qu'elles sont*/
    if($nb_utilisateur <= 5 ){
        $i = 0;
        foreach($utilisateurs as $u){
            echo "u = $u ";
            if($u != 0){
                $pdo->query("UPDATE utilisateur SET fkPersonnalite = $personnalite[$i] WHERE idUtilisateur = $u");
            }
            $i++;
        }
        header('location: creation_partie.php');
    }
    /*Sinon compte le nombre de participant qu'il doit y avoir dans chaque equipe*/
    else{
        $equilibrage = intdiv($nb_utilisateur,4);
        $reste = $nb_utilisateur % 4;
        echo "reste = $reste</br>";
        $array = array($p1 => $equilibrage, $p2 => $equilibrage, $p3 => $equilibrage, $p4 => $equilibrage);
        foreach($array as $key => $p){
            if ($reste > 0){
                $array[$key]++;
                $reste--;
            }
        }
        foreach($array as $key => $p){
            echo "{$key} => {$p} ";
        }
        
        /* Melange des utilisateurs */
        shuffle($utilisateurs);
        /*Repartition des personnalites entre les utilisateurs*/
        foreach($utilisateurs as $u){
            if($u != 0){
                if($array[$p1] > 0){
                    $pdo->query("UPDATE utilisateur SET fkPersonnalite = $p1 WHERE idUtilisateur = $u");
                    $array[$p1]--;
                }
                else if ($array[$p2] > 0){
                    $pdo->query("UPDATE utilisateur SET fkPersonnalite = $p2 WHERE idUtilisateur = $u");
                    $array[$p2]--;
                }
                else if ($array[$p3] > 0){
                    $pdo->query("UPDATE utilisateur SET fkPersonnalite = $p3 WHERE idUtilisateur = $u");
                    $array[$p3]--;
                }
                else if ($array[$p4] > 0){
                    $pdo->query("UPDATE utilisateur SET fkPersonnalite = $p4 WHERE idUtilisateur = $u");
                    $array[$p4]--;
                }
            }
        }
        header('location: creation_partie.php');
    }
?>
