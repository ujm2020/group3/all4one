<?php
/*****************************************************/
/***              Projet tutoré L3 info            ***/
/***                      Menu                     ***/
/***  Clain-Januel-Laroche-Moulin-Verdun-Buzenet   ***/
/*****************************************************/

    session_start();
    include 'connexion.inc.php';
    $pdo=connex('BD_groupe3');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Menu</title>
        <link rel="stylesheet" type="text/css" href="../css/debut.css">
    </head>
    <body>
        <?php
            if(isset($_SESSION['pseudo']) && strcmp($_SESSION['pseudo'], "admin") == 0){
                $pdo->query("UPDATE partie SET lance = 0");

                /* compte le nombre de personne connecter */
                $temps_session = 3;
                $temps_actuel = date("U");
                $temps_connecter = $temps_actuel - $temps_session;

                $req = $pdo->prepare("SELECT * FROM utilisateur where time > :tmp_co");
                $req->bindParam(':tmp_co', $temps_connecter, PDO::PARAM_INT);
                $req->execute();
                $nb_utilisateur = $req->rowCount();


                $aff_menu = '<legend>Menu</legend>';
                $aff_menu.= '<div class="global_enter2">';
                $aff_menu.= '<div class="enter"><p>Nombre de personne en attente '.$nb_utilisateur.'</p></div>';
                $aff_menu.= '<div class="enter"><a href="menu.php"><input type="button" value="compte inscription"></a>';
                $aff_menu.= '<a href="repartition.php"><input type="button" value="nouvelle partie"></a>';
                $aff_menu.= '<a href="lancer_partie.php"><input type="button" value="charger"></a>';
                $aff_menu.= '<a href="logout.php"><input type="button" value="deconnexion"></a></div>';
                $aff_menu.= '<div>';
                echo $aff_menu;
            }else{
                $pas_co = '<div class="deja_co">';
                $pas_co.= '<p>connecte vous</p>';
                $pas_co.= '<a href="connexion.php"><input type="button" value="connexion"></a>';
                $pas_co.= '<a href="inscription.php"><input type="button" value="inscription"></a>';
                $pas_co.= '</div>';
                echo $pas_co;
            }
        ?>
    </body>
</html>
