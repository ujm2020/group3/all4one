<?php
/*************************************************************/
/***                 Projet Tutoré L3-Info                 ***/ 
/***               Suite du jeu -- Téléphone               ***/
/***           Clain-Januel-Laroche-Moulin-Verdun-Buzenet  ***/
/*************************************************************/
    session_start();
    /**
     * Connexion à la bdd
     */

    $bdd = 'BD_groupe3';
    include_once('connexion.inc.php');
    $pdo = connex($bdd);
    /**
     * Fin connexion
     */

    /**
     * Zone requete en BDD     
     */
    try{
        $req = $pdo->prepare('SELECT * FROM partie ORDER BY idPartie DESC;');
        $req->execute();
        $value=$req->fetchAll(PDO::FETCH_ASSOC);
        $salle=$value[0][fkSalle];

        $req = $pdo->prepare('SELECT * FROM salle WHERE idSalle=:salle;');
        $req->bindParam(':salle', $salle, PDO::PARAM_INT);
        $req->execute();
        $value=$req->fetchAll(PDO::FETCH_ASSOC);

        if($value[0][action] == 1){
            /**
             * Récup salle + Check si action disponible
             */
            $req = $pdo->prepare('SELECT idAction, label, fkSalle FROM action WHERE fkSalle=:salle;');
            $req->bindParam(':salle', $salle, PDO::PARAM_INT);
            $req->execute();

            $val = $req->fetchAll(PDO::FETCH_ASSOC);

            //On fait passer le type au js smartphone
            $val[0]["action"] = 1;
            $data = json_encode($val);
        }else{
            $req = $pdo->prepare('SELECT idChoix , label, fkSalle FROM choix WHERE fkSalle=:salle;');
            $req->bindParam(':salle', $salle, PDO::PARAM_INT);
            $req->execute();

            $val = $req->fetchAll(PDO::FETCH_ASSOC);
            $val[0]["action"]=0;
            $data = json_encode($val);
            //On fait passer le type au js smartphone

        }
        echo $data;
    }catch(Exception $e){
        echo $e;
        die();
    }
?>
