-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mar 09 Juin 2020 à 11:55
-- Version du serveur :  5.7.30-0ubuntu0.18.04.1
-- Version de PHP :  7.2.24-0ubuntu0.18.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `BD_groupe3`
--

DELIMITER $$
--
-- Procédures
--
CREATE DEFINER=`groupe3`@`localhost` PROCEDURE `reset_action` ()  BEGIN
	UPDATE salle SET action=1 WHERE action_h=1;
END$$

CREATE DEFINER=`groupe3`@`localhost` PROCEDURE `reset_reponse` ()  BEGIN
	DELETE FROM `reponse`;
END$$

CREATE DEFINER=`groupe3`@`localhost` PROCEDURE `reset_visited` ()  BEGIN
	UPDATE salle SET visited=0; 
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `action`
--

CREATE TABLE `action` (
  `idAction` int(11) NOT NULL,
  `fkSalle` int(11) NOT NULL,
  `texte` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `degat` int(11) NOT NULL,
  `idBonus` int(11) NOT NULL,
  `gain_perte` int(11) NOT NULL,
  `objet` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `action`
--

INSERT INTO `action` (`idAction`, `fkSalle`, `texte`, `label`, `degat`, `idBonus`, `gain_perte`, `objet`) VALUES
(30, 2, 'arme_1_aller.txt', 'Avancer vers la lumiere', 0, 100, 4, 'arme_1.png'),
(31, 2, 'arme_1_ignorer.txt', 'Ignorer ces lueurs', 0, 100, 0, 'vide'),
(32, 3, 'arme_2_laisser.txt', 'Laisser le corps', 0, 1, 0, 'vide'),
(33, 3, 'arme_2_voler.txt', 'Dépouiller le cadavre', 0, 100, 4, 'arme_2.png'),
(34, 4, 'arme_3_laisser.txt', 'Laisser la lance', 0, 100, 0, 'vide'),
(35, 4, 'arme_3_voler.txt', 'Dérober l\'arme', 0, 100, 4, 'arme_3.png'),
(36, 6, 'rencontre_barde_ecouter.txt', 'Ecouter la barde', 0, 100, 0, 'vide'),
(37, 6, 'rencontre_barde_ignorer.txt', 'Ignorer sa supplique', 0, 100, 0, 'vide'),
(38, 7, 'combat_1_battre.txt', 'Se battre', 0, 10, 1, 'bourse.png'),
(39, 7, 'combat_1_payer.txt', 'Payer un tribut', 0, 100, 3, 'bourse.png'),
(40, 8, 'combat_2_battre.txt', 'Se battre', 0, 10, 4, 'massue.png'),
(41, 8, 'combat_2_fuir.txt', 'Fuir au plus vite', 0, 4, 2, 'tous'),
(42, 11, 'rencontre_monstre_combat.txt', 'En garde! Monstre!', 0, 10, 1, 'torche.png'),
(43, 11, 'rencontre_monstre_fuite.txt', 'Au secouuuurs!', 0, 2, 0, 'vide'),
(44, 12, 'auberge_alcool.txt', 'Un verre, ou deux...', 0, 2, 3, 'bourse.png'),
(45, 12, 'auberge_parler.txt', 'Comment vont les affaires?', 0, 100, 0, 'vide'),
(46, 12, 'auberge_repas.txt', 'Du pain l\'ami!', 0, 3, 3, 'bourse.png'),
(47, 14, 'bloqueur_mur_1_hache.txt', 'Marteler l\'arbre', 0, 100, 0, 'vide'),
(48, 15, 'rencontre_pnj_vieille_femme_1_ignorer.txt', 'L\'ignorer', 0, 100, 0, 'vide'),
(49, 15, 'rencontre_pnj_vieille_femme_1_parler.txt', 'Aller lui parler', 0, 100, 1, 'carte.png'),
(50, 16, 'coffre_1_ignorer.txt', 'Ne pas ouvrir', 0, 100, 0, 'vide'),
(51, 16, 'coffre_1_ouvrir.txt', 'Ouvrir', 0, 10, 0, 'vide'),
(52, 17, 'rencontre_pnj_chevalier_1_combattre.txt', 'Tu me sous-estimes..combattre', 0, 10, 0, 'vide'),
(53, 17, 'rencontre_pnj_chevalier_1_parler.txt', 'Le raisonner', 0, 100, 0, 'vide'),
(54, 18, 'rencontre_pnj_marchand_1_bouclier.txt', 'Ce bouclier pourra me protéger', 0, 100, 3, 'bouclier.png'),
(55, 18, 'rencontre_pnj_marchand_1_sabre.txt', 'Avec ce sabre je suis invincible', 0, 100, 3, 'sabre.png'),
(56, 18, 'rencontre_pnj_marchand_1_heaume.txt', 'Un heaume... pourquoi pas', 0, 100, 3, 'heaume.png'),
(57, 19, 'rencontre_pnj_soldat_1_action.txt', 'Se battre', 0, 5, 0, 'vide'),
(58, 19, 'rencontre_pnj_soldat_1_parler.txt', 'Rester calme et lui parler', 0, 6, 0, 'vide'),
(59, 18, 'rencontre_pnj_marchand_1_ignorer.txt', 'Rien ne vous intéresse', 0, 100, 0, 'vide'),
(60, 32, 'genie_or.txt', 'Beaucoup d\'or', 0, 100, 1, 'bourse.png'),
(61, 32, 'genie_memoire.txt', 'Savoir ce qu\'il s\'est passé', 0, 1, 0, 'vide'),
(62, 32, 'genie_maladie.txt', 'Soigner votre maladie', 0, 100, 0, 'vide'),
(63, 32, 'genie_steak_frite.txt', 'Un steak frite lidl', 0, 3, 0, 'vide'),
(64, 37, 'puit_regen_boire.txt', 'Boire', 0, 7, 0, 'vide'),
(65, 37, 'puit_regen_ignorer.txt', 'Ignorer', 0, 100, 0, 'vide'),
(66, 30, 'lutin_approcher.txt', 'S\'approcher', 0, 100, 2, 'deux'),
(67, 30, 'lutin_ignorer.txt', 'Ne pas s\'approcher de la grotte', 0, 100, 0, 'vide'),
(68, 20, 'traiter_paix_ignorer.txt', 'Les ignorer', 0, 100, 0, 'vide'),
(69, 20, 'traiter_paix_parler.txt', 'Intervenir', 0, 100, 0, 'vide'),
(70, 28, 'champignons_bleu.txt', 'Manger le champignon bleu', 0, 8, 0, 'vide'),
(71, 28, 'champignons_rouge.txt', 'Manger le champignon rouge', 0, 9, 0, 'vide'),
(72, 28, 'champignons_ignorer.txt', 'S\'en aller', 0, 100, 0, 'vide'),
(73, 24, 'arme_4_ignorer.txt', 'Ignorer l\'arme', 0, 100, 0, 'vide'),
(74, 24, 'arme_4_ramasser.txt', 'Ramasser l\'arme', 0, 100, 1, 'arme_4.png'),
(75, 36, 'combat_5_combat.txt', 'Combattre le cobra', 0, 10, 0, 'vide'),
(76, 36, 'combat_5_fuir.txt', 'Fuir au plus vite', 0, 100, 0, 'vide'),
(77, 23, 'rencontre_pnj_payer.txt', 'Payer le montagnard', 0, 100, 3, 'bourse.png'),
(78, 23, 'rencontre_pnj_ignorer.txt', 'Ignorer et partir', 0, 100, 0, 'vide'),
(79, 26, 'rencontre_pnj_mineur_ramasser.txt', 'Récupérer la pioche', 0, 100, 4, 'pioche.png'),
(80, 26, 'rencontre_pnj_mineur_ignorer.txt', 'Ne pas ramasser', 0, 100, 0, 'vide'),
(81, 33, 'trois_rochers_ignorers.txt', 'Ignorer', 0, 100, 0, 'vide'),
(82, 33, 'trois_rochers_prendre.txt', 'Tenter de retirer l\'épée', 0, 100, 4, 'epee_rocher.png'),
(83, 25, 'nain_combat.txt', 'Se battre', 0, 11, 0, 'vide'),
(84, 25, 'nain_fuir.txt', 'Fuir le combat', 0, 100, 0, 'vide'),
(85, 41, 'enigme_trois.txt', 'Il n\'y a que trois personnes', 0, 100, 0, 'vide'),
(86, 41, 'enigme_fantome.txt', 'Il y a un fantôme', 0, 4, 0, 'vide'),
(87, 41, 'enigme_frapper.txt', 'Frapper la vieille', 0, 4, 0, 'vide'),
(88, 42, 'event_fin_2_bourrin.txt', 'Dégainer et foncer sur lui', 0, 11, 0, 'vide'),
(89, 42, 'event_fin_2_dompter.txt', 'Essayer de le dompter', 0, 12, 0, 'vide'),
(90, 42, 'event_fin_2_boule.txt', 'Pleurer, paniquer ...', 0, 2, 0, 'vide'),
(91, 34, 'combat_4_combattre.txt', 'Accepter', 0, 11, 0, 'vide'),
(92, 34, 'combat_4_raisonner.txt', 'Le raisonner', 0, 100, 0, 'vide'),
(93, 9, 'hache_prendre.txt', 'Bizarre mais je prends', 0, 100, 1, 'hache.png'),
(94, 9, 'hache_refuser.txt', 'Trop bizarre je passe', 0, 100, 0, 'vide'),
(95, 31, 'combat_3_combattre.txt', 'Se battre', 0, 10, 1, 'bourse.png'),
(96, 31, 'combat_3_payer.txt', 'Payer la taxe', 0, 100, 2, 'bourse.png'),
(97, 38, 'combat_6_combattre.txt', 'Se battre', 0, 2, 0, 'vide'),
(98, 38, 'combat_6_fuir.txt', 'Fuir', 0, 100, 0, 'vide');

-- --------------------------------------------------------

--
-- Structure de la table `bonus`
--

CREATE TABLE `bonus` (
  `idBonus` int(11) NOT NULL,
  `sante_phy` int(11) DEFAULT NULL,
  `sante_ment` int(11) DEFAULT NULL,
  `bonus_score` int(11) DEFAULT NULL,
  `fkPerso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `bonus`
--

INSERT INTO `bonus` (`idBonus`, `sante_phy`, `sante_ment`, `bonus_score`, `fkPerso`) VALUES
(1, 0, 2, 1, NULL),
(2, 0, 2, -1, NULL),
(3, 5, 0, 1, NULL),
(4, 2, 2, -1, NULL),
(5, 0, 1, -1, NULL),
(6, 0, 1, 1, NULL),
(7, 2, 2, 1, NULL),
(8, 2, 3, -1, NULL),
(9, 3, 2, 1, NULL),
(10, 4, 0, -1, NULL),
(11, 8, 0, -1, NULL),
(12, 4, 2, -1, NULL),
(100, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `choix`
--

CREATE TABLE `choix` (
  `idChoix` int(11) NOT NULL,
  `fkSalle` int(11) DEFAULT NULL,
  `fkSalleSuiv` int(11) DEFAULT NULL,
  `fkBonus` int(11) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `numChemin` int(11) NOT NULL,
  `numPattern` int(11) NOT NULL,
  `pvRetirer` int(11) NOT NULL,
  `smRetirer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `choix`
--

INSERT INTO `choix` (`idChoix`, `fkSalle`, `fkSalleSuiv`, `fkBonus`, `label`, `numChemin`, `numPattern`, `pvRetirer`, `smRetirer`) VALUES
(1, 1, 2, 1, 'Aller au Nord', 61, 2, 0, 0),
(2, 1, 4, 1, 'Aller au Sud', 62, 3, 0, 0),
(3, 1, 3, 1, 'Aller a l\'Est', 60, 1, 0, 0),
(4, 2, 6, 1, 'Aller au Nord', 66, 2, 0, 0),
(5, 2, 7, 1, 'Aller a l\'Est', 63, 1, 0, 0),
(6, 3, 7, 1, 'Aller au Nord-Est', 64, 5, 0, 0),
(7, 3, 8, 1, 'Aller a l\'Est', 65, 1, 0, 0),
(8, 4, 9, 1, 'Aller a l\'Est', 66, 1, 0, 0),
(9, 4, 5, 1, 'Aller au Sud', 69, 8, 0, 0),
(10, 6, 11, 1, 'Aller a l\'Est', 73, 1, 0, 0),
(11, 6, 7, 1, 'Aller au Sud', 70, 4, 0, 0),
(12, 7, 12, 1, 'Aller a l\'Est', 74, 1, 0, 0),
(13, 7, 6, 1, 'Aller au Nord', 70, 0, 0, 0),
(14, 8, 13, 1, 'Aller a l\'Est', 75, 1, 0, 0),
(15, 8, 9, 1, 'Aller au Sud', 71, 4, 0, 0),
(16, 9, 13, 1, 'Aller au Nord-Est', 76, 13, 0, 0),
(17, 9, 14, 1, 'Aller a l\'Est', 77, 1, 0, 0),
(18, 9, 8, 1, 'Aller au Nord', 71, 0, 0, 0),
(19, 10, 14, 1, 'Aller au Nord-Est', 72, 5, 0, 0),
(20, 10, 15, 1, 'Aller a l\'Est', 78, 1, 0, 0),
(21, 5, 15, 1, 'Aller au Nord-Est', 67, 14, 0, 0),
(22, 5, 16, 1, 'Aller a l\'Est', 68, 12, 0, 0),
(23, 11, 17, 1, 'Aller a l\'Est', 79, 1, 0, 0),
(24, 12, 17, 1, 'Aller au Nord-Est', 80, 5, 0, 0),
(25, 12, 18, 1, 'Aller a l\'Est', 81, 1, 0, 0),
(26, 13, 19, 1, 'Aller a l\'Est', 82, 1, 0, 0),
(27, 13, 9, 1, 'Aller au Sud-Ouest', 76, 0, 0, 0),
(28, 14, 19, 1, 'Aller au Nord-Est', 83, 5, 0, 0),
(29, 14, 10, 1, 'Aller au Sud-Ouest', 72, 0, 0, 0),
(30, 15, 20, 1, 'Aller a l\'Est', 84, 1, 0, 0),
(31, 15, 5, 1, 'Aller a l\'Est', 67, 0, 0, 0),
(32, 16, 20, 1, 'Aller au Nord-Est', 85, 9, 0, 0),
(33, 17, 21, 1, 'Aller au Sud-Est', 86, 6, 0, 0),
(34, 17, 12, 1, 'Aller au Sud-Ouest', 80, 0, 0, 0),
(35, 18, 21, 1, 'Aller a l\'Est', 87, 1, 0, 0),
(36, 18, 22, 1, 'Aller au Sud-Est', 88, 11, 0, 0),
(37, 19, 22, 1, 'Aller a l\'Est', 89, 1, 0, 0),
(38, 19, 20, 1, 'Aller au Sud', 104, 18, 0, 0),
(39, 20, 19, 1, 'Aller au Nord', 104, 0, 0, 0),
(40, 20, 22, 1, 'Aller au Nord-Est', 90, 7, 0, 0),
(41, 21, 23, 1, 'Aller au Nord', 91, 15, 0, 0),
(42, 21, 24, 1, 'Aller a l\'Est', 92, 1, 0, 0),
(43, 22, 25, 1, 'Aller a l\'Est', 93, 1, 0, 0),
(44, 22, 26, 1, 'Aller au Sud', 94, 16, 0, 0),
(45, 23, 28, 1, 'Aller au Nord', 95, 2, 0, 0),
(46, 23, 29, 1, 'Aller a l\'Est', 96, 1, 0, 0),
(47, 23, 30, 1, 'Aller au Sud-Est', 97, 11, 0, 0),
(48, 24, 30, 1, 'Aller a l\'Est', 98, 1, 0, 0),
(49, 24, 25, 1, 'Aller au Sud', 99, 4, 0, 0),
(50, 25, 24, 1, 'Aller au Nord', 99, 0, 0, 0),
(51, 25, 31, 1, 'Aller a l\'Est', 100, 1, 0, 0),
(52, 26, 32, 1, 'Aller a l\'Est', 101, 1, 0, 0),
(53, 26, 33, 1, 'Aller au Sud-Est', 102, 17, 0, 0),
(54, 27, 33, 1, 'Aller a l\'Est', 103, 1, 0, 0),
(55, 27, 34, 1, 'Aller au Sud-Est', 105, 3, 0, 0),
(56, 28, 35, 1, 'Aller a l\'Est', 106, 1, 0, 0),
(57, 28, 29, 1, 'Aller au Sud', 107, 4, 0, 0),
(58, 29, 28, 1, 'Aller au Nord', 107, 0, 0, 0),
(59, 29, 36, 1, 'Aller a l\'Est', 108, 1, 0, 0),
(60, 30, 36, 1, 'Aller au Nord-Est', 109, 19, 0, 0),
(61, 31, 37, 1, 'Aller au Nord-Est', 110, 10, 0, 0),
(62, 31, 38, 1, 'Aller a l\'Est', 111, 1, 0, 0),
(63, 32, 38, 1, 'Aller au Nord-Est', 112, 5, 0, 0),
(64, 32, 39, 1, 'Aller a l\'Est', 113, 1, 0, 0),
(65, 32, 33, 1, 'Aller au Sud', 114, 4, 0, 0),
(66, 33, 32, 1, 'Aller au Nord', 114, 0, 0, 0),
(67, 33, 39, 1, 'Aller au Nord-Est', 115, 9, 0, 0),
(68, 33, 34, 1, 'Aller au Sud', 116, 4, 0, 0),
(69, 34, 33, 1, 'Aller au Nord', 116, 0, 0, 0),
(70, 34, 40, 1, 'Aller a l\'Est', 117, 1, 0, 0),
(71, 35, 28, 1, 'Aller a l\'Ouest', 106, 0, 0, 0),
(72, 36, 41, 1, 'Aller au Sud-Est', 126, 6, 0, 0),
(73, 36, 37, 1, 'Aller au Sud', 118, 4, 0, 0),
(74, 37, 36, 1, 'Aller au Nord', 118, 0, 0, 0),
(75, 37, 41, 1, 'Aller a l\'Est', 119, 1, 0, 0),
(76, 37, 38, 1, 'Aller au Sud', 120, 4, 0, 0),
(77, 38, 37, 1, 'Aller au Nord', 120, 0, 0, 0),
(78, 38, 41, 1, 'Aller au Nord-Est', 127, 19, 0, 0),
(79, 39, 42, 1, 'Aller a l\'Est', 121, 1, 0, 0),
(80, 40, 34, 1, 'Aller a l\'Ouest', 117, 0, 0, 0),
(81, 40, 43, 1, 'Aller a l\'Est', 122, 1, 0, 0),
(82, 41, 42, 1, 'Aller au Sud', 123, 8, 0, 0),
(83, 41, 44, 1, 'Aller au Sud-Est', 124, 6, 0, 0),
(84, 42, 41, 1, 'Aller au Nord', 123, 0, 0, 0),
(85, 42, 44, 1, 'Aller au Nord-Est', 125, 9, 0, 0),
(86, 43, 40, 1, 'Aller a l\'Ouest', 122, 0, 0, 0),
(87, 4, 10, 1, 'Aller au Sud-Est', 126, 16, 0, 0),
(88, 26, 27, 1, 'Aller au Sud', 127, 4, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `partie`
--

CREATE TABLE `partie` (
  `idPartie` int(11) NOT NULL,
  `pv` int(11) DEFAULT '20',
  `santeMental` int(11) DEFAULT '20',
  `fkSalle` int(11) DEFAULT '1',
  `temps` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lance` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `partie`
--

INSERT INTO `partie` (`idPartie`, `pv`, `santeMental`, `fkSalle`, `temps`, `lance`) VALUES
(1, 20, 19, 2, '2020-06-08 12:31:03', 0),
(2, 20, 19, NULL, '2020-06-08 12:33:59', 0),
(3, 20, 19, 2, '2020-06-08 12:34:58', 0),
(4, 20, 19, 2, '2020-06-08 12:43:43', 0),
(5, 20, 19, 2, '2020-06-08 12:50:19', 0),
(6, 20, 10, NULL, '2020-06-08 13:01:26', 0),
(7, 20, 19, 2, '2020-06-08 13:02:14', 0),
(8, 20, 20, 1, '2020-06-08 13:12:05', 0),
(9, 20, 19, 2, '2020-06-08 13:13:02', 0),
(10, 20, 19, 2, '2020-06-08 13:17:21', 0),
(11, 20, 16, 6, '2020-06-08 13:22:11', 0),
(12, 20, 20, 1, '2020-06-08 13:35:34', 0),
(13, 20, 20, 1, '2020-06-08 13:38:09', 0),
(14, 20, 20, 1, '2020-06-08 13:41:30', 0),
(15, 20, 20, 1, '2020-06-08 13:45:35', 0),
(16, 17, 14, NULL, '2020-06-08 13:51:17', 0),
(17, 20, 18, NULL, '2020-06-08 13:53:50', 0),
(18, 18, 10, NULL, '2020-06-08 14:03:56', 0),
(19, 20, 11, NULL, '2020-06-08 14:11:31', 0),
(20, 20, 19, NULL, '2020-06-08 14:15:06', 0),
(21, 20, 19, 3, '2020-06-08 14:16:07', 0),
(22, 20, -1, NULL, '2020-06-08 14:50:45', 0),
(23, 20, 20, 1, '2020-06-08 14:50:46', 0),
(24, 17, 8, NULL, '2020-06-08 15:03:51', 0),
(25, 20, 16, 20, '2020-06-08 15:12:19', 0),
(26, 20, 20, 1, '2020-06-08 15:12:24', 0),
(27, 17, 18, 7, '2020-06-08 15:15:14', 0),
(28, 18, 18, 8, '2020-06-08 15:27:15', 0),
(29, 19, 12, 26, '2020-06-08 15:34:40', 0),
(30, 19, 15, NULL, '2020-06-08 15:39:19', 0),
(31, 19, 17, 9, '2020-06-08 15:46:09', 0),
(32, 18, 18, 8, '2020-06-08 15:49:06', 0),
(33, 20, 19, NULL, '2020-06-08 15:54:02', 0),
(34, 20, 19, 3, '2020-06-08 15:54:57', 0),
(35, 20, 16, NULL, '2020-06-08 15:59:08', 0),
(36, 11, 8, NULL, '2020-06-08 16:20:48', 0),
(37, 20, 20, 1, '2020-06-08 16:41:52', 0),
(38, 20, 20, 1, '2020-06-08 16:48:06', 0),
(39, 20, 13, NULL, '2020-06-08 16:59:28', 0),
(40, 20, 20, 1, '2020-06-08 17:00:57', 0),
(41, 20, 19, 3, '2020-06-08 17:05:35', 0),
(42, 20, 20, 1, '2020-06-08 17:08:54', 0),
(43, 20, 20, 1, '2020-06-08 17:12:10', 0),
(44, 20, 20, 1, '2020-06-08 17:13:33', 0),
(45, 20, 20, 1, '2020-06-08 17:14:01', 0),
(46, 20, 20, 1, '2020-06-08 17:17:02', 0),
(47, 20, 19, 3, '2020-06-08 17:19:45', 0),
(48, 20, 19, 2, '2020-06-08 17:20:30', 0),
(49, 17, 14, NULL, '2020-06-09 08:23:27', 0),
(50, 20, 13, NULL, '2020-06-09 08:29:18', 0),
(51, 20, 9, NULL, '2020-06-09 08:47:17', 0),
(52, 20, 20, 1, '2020-06-09 08:51:58', 0),
(53, 19, 12, 31, '2020-06-09 09:01:50', 0),
(54, 16, 13, NULL, '2020-06-09 09:09:27', 0),
(55, 20, 20, 1, '2020-06-09 09:17:32', 0),
(56, 20, 18, 7, '2020-06-09 09:20:29', 0),
(57, 20, 20, 1, '2020-06-09 09:20:33', 0),
(58, 20, 18, 8, '2020-06-09 09:22:30', 0),
(59, 12, 12, NULL, '2020-06-09 09:34:05', 0),
(60, 20, 17, NULL, '2020-06-09 09:39:12', 0),
(61, 20, 15, 22, '2020-06-09 09:44:06', 0),
(62, 20, 19, NULL, '2020-06-09 09:47:18', 0),
(63, 18, 18, 8, '2020-06-09 09:51:18', 0),
(64, 20, 18, 6, '2020-06-09 09:54:22', 0),
(65, 20, 20, 1, '2020-06-09 09:54:53', 1);

-- --------------------------------------------------------

--
-- Structure de la table `personnalite`
--

CREATE TABLE `personnalite` (
  `idPerso` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `actif` tinyint(4) DEFAULT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `personnalite`
--

INSERT INTO `personnalite` (`idPerso`, `label`, `score`, `actif`, `image`) VALUES
(0, 'Pas de perso', 0, 0, 'none.png'),
(1, 'Combattant', 0, 0, 'combattant.png'),
(2, 'Pacifiste', 0, 0, 'pacifiste.png'),
(3, 'Collectionneur', 0, 0, 'collectionneur.png'),
(4, 'Explorateur', 0, 0, 'explorateur.png');

-- --------------------------------------------------------

--
-- Structure de la table `reponse`
--

CREATE TABLE `reponse` (
  `idReponse` int(11) NOT NULL,
  `valeur` varchar(255) DEFAULT NULL,
  `fkSalle` int(11) NOT NULL,
  `fkChoix` int(11) DEFAULT NULL,
  `fkAction` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `reponse`
--

INSERT INTO `reponse` (`idReponse`, `valeur`, `fkSalle`, `fkChoix`, `fkAction`) VALUES
(2471, 'Aller au Sud', 6, 11, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

CREATE TABLE `salle` (
  `idSalle` int(11) NOT NULL,
  `histoire` varchar(255) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `son` varchar(255) NOT NULL,
  `action` int(11) NOT NULL DEFAULT '1',
  `action_h` tinyint(4) NOT NULL DEFAULT '1',
  `visited` int(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `salle`
--

INSERT INTO `salle` (`idSalle`, `histoire`, `image_name`, `son`, `action`, `action_h`, `visited`) VALUES
(1, 'depart.txt', 'depart.png', 'test.mp3', 0, 0, 0),
(2, 'arme_1.txt', 'arme_1.png', 'test2.mp3', 1, 1, 0),
(3, 'arme_2.txt', 'arme_2.png', '', 1, 1, 0),
(4, 'arme_3.txt', 'arme_3.png', '', 1, 1, 0),
(5, 'pnj_coffre.txt', 'pnj_coffre.png', '', 0, 0, 0),
(6, 'rencontre_barde.txt', 'barde.gif', '', 1, 1, 0),
(7, 'combat_1.txt', 'combat_1.png', '', 1, 1, 0),
(8, 'combat_2.txt', 'combat_2.png', '', 1, 1, 0),
(9, 'hache.txt', 'hache.png', '', 1, 1, 0),
(10, 'carrefour_vide_1.txt', 'carrefour_vide_1.png', '', 0, 0, 0),
(11, 'rencontre_monstre.txt', 'rencontre_monstre.png', '', 1, 1, 0),
(12, 'auberge.txt', 'auberge.png', '', 1, 1, 0),
(13, 'carrefour_vide_2.txt', 'carrefour_vide_2.png', '', 0, 0, 0),
(14, 'bloqueur_mur_1.txt', 'bloqueur_mur_1.png', '', 1, 1, 0),
(15, 'rencontre_pnj_vieille_femme_1.txt', 'rencontre_pnj_vieille_femme_1.png', '', 1, 1, 0),
(16, 'coffre_1.txt', 'coffre_1.png', '', 1, 1, 0),
(17, 'rencontre_pnj_chevalier_1.txt', 'rencontre_pnj_chevalier_1.png', '', 1, 1, 0),
(18, 'rencontre_pnj_marchand_1.txt', 'rencontre_pnj_marchand_1.png', '', 1, 1, 0),
(19, 'rencontre_pnj_soldat_1.txt', 'rencontre_pnj_soldat_1.png', '', 1, 1, 0),
(20, 'traiter_paix.txt', 'traiter_paix.png', '', 1, 1, 0),
(21, 'entree_grotte_1.txt', 'entree_grotte_1.png', '', 0, 0, 0),
(22, 'entree_grotte_2.txt', 'entree_grotte_2.png', '', 0, 0, 0),
(23, 'rencontre_pnj.txt', 'rencontre_pnj.png', '', 1, 1, 0),
(24, 'arme_4.txt', 'arme_4.png', '', 1, 1, 0),
(25, 'nain.txt', 'nain.png', '', 1, 1, 0),
(26, 'rencontre_pnj_mineur.txt', 'mineur.gif', '', 1, 1, 0),
(27, 'rencontre_pnj_2.txt', 'rencontre_pnj_2.png', '', 1, 1, 0),
(28, 'champignons.txt', 'champignons.png', '', 1, 1, 0),
(29, 'carrefour_vide_3.txt', 'carrefour_vide_3.png', '', 1, 1, 0),
(30, 'lutin.txt', 'lutin.png', '', 1, 1, 0),
(31, 'combat_3.txt', 'combat_3.png', '', 1, 1, 0),
(32, 'genie.txt', 'genie.png', '', 1, 1, 0),
(33, 'trois_rochers.txt', 'trois_rochers.png', '', 1, 1, 0),
(34, 'combat_4.txt', 'combat_4.png', '', 1, 1, 0),
(35, 'salle_cachee.txt', 'salle_cachee.png', '', 1, 1, 0),
(36, 'combat_5.txt', 'combat_5.png', '', 1, 1, 0),
(37, 'puit_regen.txt', 'puit_regen.png', '', 1, 1, 0),
(38, 'combat_6.txt', 'giant_spider.gif', '', 1, 1, 0),
(39, 'cadavre.txt', 'cadavre.png', '', 0, 0, 0),
(40, 'carrefour_vide_4.txt', 'carrefour_vide_4.png', '', 1, 1, 0),
(41, 'enigme.txt', 'enigme.png', '', 1, 1, 0),
(42, 'event_fin_2.txt', 'event_fin_2.png', '', 1, 1, 0),
(43, 'coffre_2.txt', 'coffre_2.png', '', 1, 1, 0),
(44, 'fin_remede.txt', 'fin_remede.png', '', 1, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `score`
--

CREATE TABLE `score` (
  `idScore` int(11) NOT NULL,
  `fkPersonnalite` int(11) NOT NULL,
  `Score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `score`
--

INSERT INTO `score` (`idScore`, `fkPersonnalite`, `Score`) VALUES
(101, 3, 0),
(102, 1, 0),
(103, 4, 0),
(104, 2, 0);

-- --------------------------------------------------------

--
-- Structure de la table `scoring`
--

CREATE TABLE `scoring` (
  `fkPersonnalite` int(11) NOT NULL,
  `fkChoix` int(11) NOT NULL,
  `nbPoint` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `scoring`
--

INSERT INTO `scoring` (`fkPersonnalite`, `fkChoix`, `nbPoint`) VALUES
(1, 5, 3),
(1, 40, 2),
(2, 4, 3),
(2, 9, 3),
(2, 41, 4),
(3, 22, 4),
(3, 40, 3),
(4, 1, 1),
(4, 2, 1),
(4, 3, 1),
(4, 4, 1),
(4, 5, 1),
(4, 6, 1),
(4, 7, 1),
(4, 8, 1),
(4, 9, 1),
(4, 10, 1),
(4, 11, 1),
(4, 12, 1),
(4, 13, 1),
(4, 14, 1),
(4, 15, 1),
(4, 16, 1),
(4, 17, 1),
(4, 18, 1),
(4, 19, 1),
(4, 20, 1),
(4, 21, 1),
(4, 22, 2),
(4, 23, 1),
(4, 24, 1),
(4, 25, 1),
(4, 26, 1),
(4, 27, 1),
(4, 28, 1),
(4, 29, 1),
(4, 30, 1),
(4, 31, 1),
(4, 32, 1),
(4, 33, 1),
(4, 34, 1),
(4, 35, 1),
(4, 36, 1),
(4, 37, 1),
(4, 38, 1),
(4, 39, 1),
(4, 40, 1),
(4, 41, 1),
(4, 42, 1),
(4, 43, 1),
(4, 44, 1),
(4, 45, 1),
(4, 46, 1),
(4, 47, 1),
(4, 48, 1),
(4, 49, 1),
(4, 50, 1),
(4, 51, 1),
(4, 52, 1),
(4, 53, 1),
(4, 54, 1),
(4, 55, 1),
(4, 56, 1),
(4, 57, 1),
(4, 58, 1),
(4, 59, 1),
(4, 60, 1),
(4, 61, 1),
(4, 62, 1),
(4, 63, 1),
(4, 64, 1),
(4, 65, 1),
(4, 66, 1),
(4, 67, 1),
(4, 68, 1),
(4, 69, 1),
(4, 70, 1),
(4, 71, 1),
(4, 72, 1),
(4, 73, 1),
(4, 74, 1),
(4, 75, 1),
(4, 76, 1),
(4, 77, 1),
(4, 78, 1),
(4, 79, 1),
(4, 80, 1),
(4, 81, 1),
(4, 82, 1),
(4, 83, 1),
(4, 84, 1),
(4, 85, 1),
(4, 86, 1),
(4, 87, 1),
(4, 88, 1);

-- --------------------------------------------------------

--
-- Structure de la table `scoring_action`
--

CREATE TABLE `scoring_action` (
  `fkPersonnalite` int(11) NOT NULL,
  `fkAction` int(11) NOT NULL,
  `nbPoint` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `scoring_action`
--

INSERT INTO `scoring_action` (`fkPersonnalite`, `fkAction`, `nbPoint`) VALUES
(1, 33, 5),
(1, 35, 5),
(1, 38, 3),
(1, 42, 3),
(1, 51, 2),
(1, 52, 3),
(1, 57, 3),
(1, 69, -2),
(1, 83, 2),
(1, 84, 2),
(1, 88, 3),
(1, 90, -4),
(1, 91, 2),
(1, 92, 4),
(2, 30, 3),
(2, 32, 5),
(2, 34, 5),
(2, 36, 6),
(2, 39, 3),
(2, 43, 5),
(2, 49, 4),
(2, 50, 2),
(2, 53, 4),
(2, 58, 4),
(2, 59, 5),
(2, 69, 10),
(2, 82, 2),
(2, 84, 2),
(2, 89, 5),
(3, 30, 5),
(3, 33, 3),
(3, 35, 3),
(3, 38, 1),
(3, 39, -2),
(3, 42, 3),
(3, 43, -1),
(3, 45, 3),
(3, 49, 3),
(3, 54, 2),
(3, 55, 2),
(3, 56, 2),
(3, 66, -8),
(3, 74, 1),
(3, 82, 1),
(3, 93, 2),
(4, 30, 1),
(4, 45, 2),
(4, 49, 1),
(4, 52, 2),
(4, 66, 1),
(4, 77, 2);

-- --------------------------------------------------------

--
-- Structure de la table `stuff`
--

CREATE TABLE `stuff` (
  `idStuff` int(11) NOT NULL,
  `objet` varchar(255) DEFAULT NULL,
  `arme` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `stuff`
--

INSERT INTO `stuff` (`idStuff`, `objet`, `arme`) VALUES
(1, 'bourse.png', 0),
(2, 'bourse.png', 0),
(3, 'bourse.png', 0);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `idUtilisateur` int(11) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `fkPersonnalite` int(11) NOT NULL DEFAULT '0',
  `time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`idUtilisateur`, `pseudo`, `password`, `status`, `fkPersonnalite`, `time`) VALUES
(0, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 1, 0, NULL),
(1, 'lol', '9cdfb439c7876e703e307864c9167a15', 0, 1, 1591696001);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `action`
--
ALTER TABLE `action`
  ADD PRIMARY KEY (`idAction`),
  ADD KEY `fkSalle` (`fkSalle`),
  ADD KEY `idBonus` (`idBonus`);

--
-- Index pour la table `bonus`
--
ALTER TABLE `bonus`
  ADD PRIMARY KEY (`idBonus`),
  ADD KEY `fkPerso` (`fkPerso`);

--
-- Index pour la table `choix`
--
ALTER TABLE `choix`
  ADD PRIMARY KEY (`idChoix`),
  ADD KEY `fkSalle` (`fkSalle`),
  ADD KEY `fkSalleSuiv` (`fkSalleSuiv`),
  ADD KEY `fkBonus` (`fkBonus`);

--
-- Index pour la table `partie`
--
ALTER TABLE `partie`
  ADD PRIMARY KEY (`idPartie`),
  ADD KEY `fkPersonnalite` (`fkSalle`);

--
-- Index pour la table `personnalite`
--
ALTER TABLE `personnalite`
  ADD PRIMARY KEY (`idPerso`);

--
-- Index pour la table `reponse`
--
ALTER TABLE `reponse`
  ADD PRIMARY KEY (`idReponse`),
  ADD KEY `fkSalle` (`fkSalle`),
  ADD KEY `fkChoix` (`fkChoix`),
  ADD KEY `fkAction` (`fkAction`);

--
-- Index pour la table `salle`
--
ALTER TABLE `salle`
  ADD PRIMARY KEY (`idSalle`);

--
-- Index pour la table `score`
--
ALTER TABLE `score`
  ADD PRIMARY KEY (`idScore`),
  ADD KEY `fkPersonnalite` (`fkPersonnalite`);

--
-- Index pour la table `scoring`
--
ALTER TABLE `scoring`
  ADD PRIMARY KEY (`fkPersonnalite`,`fkChoix`),
  ADD KEY `fkPersonnalite` (`fkPersonnalite`),
  ADD KEY `fkChoix` (`fkChoix`);

--
-- Index pour la table `scoring_action`
--
ALTER TABLE `scoring_action`
  ADD PRIMARY KEY (`fkPersonnalite`,`fkAction`),
  ADD KEY `fkAction` (`fkAction`);

--
-- Index pour la table `stuff`
--
ALTER TABLE `stuff`
  ADD PRIMARY KEY (`idStuff`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`idUtilisateur`),
  ADD KEY `fkPersonnalite` (`fkPersonnalite`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `action`
--
ALTER TABLE `action`
  MODIFY `idAction` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT pour la table `choix`
--
ALTER TABLE `choix`
  MODIFY `idChoix` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT pour la table `partie`
--
ALTER TABLE `partie`
  MODIFY `idPartie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT pour la table `reponse`
--
ALTER TABLE `reponse`
  MODIFY `idReponse` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2472;
--
-- AUTO_INCREMENT pour la table `score`
--
ALTER TABLE `score`
  MODIFY `idScore` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT pour la table `stuff`
--
ALTER TABLE `stuff`
  MODIFY `idStuff` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `action`
--
ALTER TABLE `action`
  ADD CONSTRAINT `action_ibfk_1` FOREIGN KEY (`fkSalle`) REFERENCES `salle` (`idSalle`),
  ADD CONSTRAINT `action_ibfk_2` FOREIGN KEY (`idBonus`) REFERENCES `bonus` (`idBonus`);

--
-- Contraintes pour la table `bonus`
--
ALTER TABLE `bonus`
  ADD CONSTRAINT `bonus_ibfk_1` FOREIGN KEY (`fkPerso`) REFERENCES `personnalite` (`idPerso`);

--
-- Contraintes pour la table `choix`
--
ALTER TABLE `choix`
  ADD CONSTRAINT `choix_ibfk_1` FOREIGN KEY (`fkSalle`) REFERENCES `salle` (`idSalle`),
  ADD CONSTRAINT `choix_ibfk_2` FOREIGN KEY (`fkSalleSuiv`) REFERENCES `salle` (`idSalle`),
  ADD CONSTRAINT `choix_ibfk_3` FOREIGN KEY (`fkBonus`) REFERENCES `bonus` (`idBonus`);

--
-- Contraintes pour la table `reponse`
--
ALTER TABLE `reponse`
  ADD CONSTRAINT `reponse_ibfk_1` FOREIGN KEY (`fkSalle`) REFERENCES `salle` (`idSalle`),
  ADD CONSTRAINT `reponse_ibfk_2` FOREIGN KEY (`fkChoix`) REFERENCES `choix` (`idChoix`),
  ADD CONSTRAINT `reponse_ibfk_3` FOREIGN KEY (`fkAction`) REFERENCES `action` (`idAction`);

--
-- Contraintes pour la table `score`
--
ALTER TABLE `score`
  ADD CONSTRAINT `PersonnaliteScore` FOREIGN KEY (`fkPersonnalite`) REFERENCES `personnalite` (`idPerso`);

--
-- Contraintes pour la table `scoring`
--
ALTER TABLE `scoring`
  ADD CONSTRAINT `Choix` FOREIGN KEY (`fkChoix`) REFERENCES `choix` (`idChoix`),
  ADD CONSTRAINT `Personnalite` FOREIGN KEY (`fkPersonnalite`) REFERENCES `personnalite` (`idPerso`);

--
-- Contraintes pour la table `scoring_action`
--
ALTER TABLE `scoring_action`
  ADD CONSTRAINT `scoring_action_ibfk_1` FOREIGN KEY (`fkPersonnalite`) REFERENCES `personnalite` (`idPerso`),
  ADD CONSTRAINT `scoring_action_ibfk_2` FOREIGN KEY (`fkAction`) REFERENCES `action` (`idAction`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
