Structure des fichiers:
=======================

Penser à bien respecter la structure suivante:

/all4one ---> Racine du projet
  |
  --> /source ---> Fichiers sources (code)
	|
        --> /php ---> (code php)
        |
        --> /js ---> (code js)
        |
        --> /css ---> (code css)
  |
  --> /txt ---> Texte à afficher (appel de la bdd)

  |
  --> /img ---> Image (Voir avec responsable Graphvisme)

  |
  --> /sound ---> Sons (Voir avec ingé son)
