Les paroles s'intensifient à mesure que vous approcher du groupe d'hommes, 
les discussions apparaissent désormais comme des ripailles et les excès de ces soldats,
si l'on en croit leur uniforme, ne sont pas que langagier.

Pendant que vous analysez la tablée, un soldat, passablement éméché, s'approche de vous.
"D'où qu'tu sors l'étranger? Tu veux d'bat'?", bafouille-t-il en levant les poings.
