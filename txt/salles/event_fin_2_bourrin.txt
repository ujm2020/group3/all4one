Vous dégainez votre arme et foncez tête baissé dans la gueule du dragon. Vous lui portez un coup, mais il l'esquive. Vous arrivez tout de même à le touché à l'aile droite. Il décide de prendre de l'élan et de recharger. Pris de peur, vous fermez les yeux. Pendant quelque secondes, un silence règne dans les montagnes. Vous ouvrez les yeux et remarquez que le dragon c'est empalé sur votre arme. 
Après avoir repris vos esprits, vous reprenez chemin. 
Quelle direction empruntez-vous ?


