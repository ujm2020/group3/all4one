La coursière est sombre et froide. Bientôt elle devient aussi malodorante.
Vous continuez malgré cela votre route et apercevez au sol plusieurs cadavres calcinés. 
La plupart d'entres eux ont été dépouillés de tout objet de valeur, cependant un long couteau brille à la ceinture de l'un des morts.
Souhaitez vous volez un mort?

