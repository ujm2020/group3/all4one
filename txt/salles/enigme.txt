Après avoir gravi plusieurs centaines de marches, une vieille femme se dresse devant vous. 
 Elle vous donne une énigme et vous menace d'une malédiction si vous répondez mal.
 "2 pères et 2 fils sont dans une chambre avec 3 lits ; chacun dort seul dans un lit ;
 il n'y a aucun lit de libre et aucun lit manquant. Comment est-ce possible ?"
