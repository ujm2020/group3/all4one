La forêt est sombre, et cette odeur insupportable vous consume l'esprit. 
Vous commencez à courir pour quitter les lieux au plus vite, lorsque deux brigands vous arrêtent.
"Doucement! Faut payer si tu veux passer par la forêt", exige l'un d'entre eux.
Ils ne semblent pas prêts à négocier, il faudra donc payer ou se battre.

