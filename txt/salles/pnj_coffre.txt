Vous arrivez aux alentours de la cahute et vous apercevez un homme mystérieux.
L'homme mystérieux s'avance vers vous. Vous l'entendez murmurer des mots. Vous l'interrompez.
"Pardon, que dites-vous ?"
Il ne vous entend pas et continue de marmonner : "A l'est, la richesse vous attend".
Voulez-vous suivre les marmonnements de l'homme mystérieux ?
